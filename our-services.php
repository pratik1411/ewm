﻿<?php include('include/site.php'); ?>
<title>Top Valued Online Marketing Services - eTraffic Web Marketing</title>
<meta name="keywords" content="SEO, Search Engine Optimisation, SEO Services, SMO, SEM, PPC, link building"/>
<meta name="description" content="Measure your results by the actual conversions.- eTraffic Web Marketing, provides excellent performance through effective SEO, SMO, SEM, PPC, content marketing, reputation management and video marketing services." />
</head>
<body>
<?php include('include/header.php'); ?>
<section class="inner-page">
<section class="page-title">
    <div class="wrapper">
        <h1>Services</h1>    
        <span class="sub-title">Explicit Online Presence Through Complete Web Marketing Services</span>     
    </div>
</section>
<section>
	<div class="wrapper">
        <p><strong>eTraffic Web Marketing</strong> has a team of Web Professionals who have years of experience in the field of Web Marketing and Design. Each <strong>eTraffic</strong> professional is carefully chosen to make sure that we get only the people who have undying passion for the job.</p>
        <p>Below is a list of the various services offered by <strong>eTraffic Web Marketing</strong> to small and medium businesses who want to maximise the power of the internet.</p>
        <ul class="services-list">
        	<li>
            	<img src="images/search-engine-optimization.png" alt="search-engine-optimization">
                <h2><a href="seo">Search Engine Optimisation</a></h2>
                <p>More popularly known as SEO, it is the process of improving/strengthening the visibility of your website in the World Wide Web through natural or organic techniques. </p>
                <a href="seo" class="read-more">Read More</a>
            </li>
            <li>
            	<img src="images/ppc-icon.png" alt="pay-per-click-management">
                <h2><a href="ppc-advertising">PPC Management</a></h2>
                <p>Being a Google Adwords Certified Partner, eTraffic Web Marketing has the team of PPC experts accomplished with extensive Adwords knowledge to assist you managing your PPC campaigns.</p>
                <a href="ppc-advertising" class="read-more">Read More</a>
            </li>
            <li class="last">
            	<img src="images/search-engine-marketing.png" alt="search-engine-marketing">
                <h2><a href="search-engine-marketing">Search Engine Marketing</a></h2>
                <p>Search Engine Marketing, or SEM, is also a form of internet marketing that utilises on-page and off-page optimisation and paid advertising to strengthen your website's visibility in search engine results pages.</p>
                <a href="search-engine-marketing" class="read-more">Read More</a>
            </li>
            <li>
            	<img src="images/socal-media-optimization.png" alt="socal-media-optimization">
                <h2><a href="social-media-optimisation">Social Media Optimisation</a></h2>
                <p>Social media certainly is a great platform that businesses can use to market their products and services online.</p>
                <a href="social-media-optimisation" class="read-more">Read More</a>
            </li>
            
            <li>
            	<img src="images/reputation-management.png" alt="reputation-management">
                <h2><a href="reputation-management">Reputation Management</a></h2>
                <p>We understand the extreme importance of keeping a good reputation for your business. The same principle should be applied online, it's important to have a positive image in the World Wide Web.</p>
                <a href="reputation-management" class="read-more">Read More</a>
            </li>
            <li class="last">
            	<img src="images/video-optimization.png" alt="video-optimization">
                <h2><a href="video-optimisation">Video Optimisation/Production</a></h2>
                <p>Video production and optimisation is one of the latest and most effective internet marketing strategies that we use here at eTraffic. We use YouTube as the platform for our online marketing video.</p>
                <a href="video-optimisation" class="read-more">Read More</a>
            </li>
            <li>
            	<img src="images/local-seo.png" alt="local-seo">
                <h2><a href="local-seo">Local SEO</a></h2>
                <p>Obtain special attention to your website in your targeted Geo Graphic location through our effective local SEO campaign. Our experts will plan out special marketing strategy to hit your local market.</p>
                <a href="local-seo" class="read-more">Read More</a>
            </li>
            
            <li>
            	<img src="images/seo-link-building.png" alt="seo-link-building">
                <h2><a href="seo-link-building">SEO Link building</a></h2>
                <p>Link Building is yet a crucial SEO technique. We have a team of expert link builders who have worked in unique industries and learned which techniques work.  We believe in only ethical practices.</p>
                <a href="seo-link-building" class="read-more">Read More</a>
            </li>
            <li class="last">
            	<img src="images/ecommerce-seo.png" alt="ecommerce-seo">
                <h2><a href="ecommerce-seo">eCommerce SEO</a></h2>
                <p>SEO for eCommerce website is certainly different. It is more than optimising category and product pages. Our eCommerce SEO Professionals are able to place your website ahead than your competitor websites. </p>
                <a href="ecommerce-seo" class="read-more">Read More</a>
            </li>
            
        </ul>
        <p><strong>eTraffic Web Marketing</strong> is your ideal partner in boosting your business through the unlimited potential offered by the internet. Get in touch with us now, and learn of the endless possibilities that await your business with <strong>eTraffic’s Internet Marketing Services!</strong></p>
        <?php include('include/like-work.php'); ?>
       </div>
</section>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>