var myLatlng    = ""
var mapColor    = "";
    

function initialize(_myLatlng, _color) {

    myLatlng        = _myLatlng
    mapColor        = _color;
    

    var circle = {
        //path: google.maps.SymbolPath.CIRCLE,
		path:google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
        fillOpacity: 1,
        fillColor: mapColor,
        strokeOpacity: 1.0,
        strokeColor: mapColor,
        strokeWeight: 1.0, 
        scale: 6
    };
		
	 var image = 'images/map-arrow.png';
   

    // Global Map Options
    var mapOptions = {
        zoom: 17,
        center: myLatlng,
        streetViewControl: false,
        // disable scroll wheel zoom 
        scrollwheel: false,
        //hide controls
        disableDefaultUI: true, 
		zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT,
            position: google.maps.ControlPosition.LEFT_TOP
        },
        navigationControlOptions: {
            style: google.maps.NavigationControlStyle.NORMAL,
            position: google.maps.ControlPosition.LEFT_TOP
        }, 
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    // Set the Market
    map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);

    var marker = new google.maps.Marker({
       position: myLatlng,
       icon: image
    });

    // Initialise the Marker
    marker.setMap(map);

    // Set the Maps style using the JSON array from http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html
	
    var styles = [
      {
        "featureType": "administrative",
        "stylers": [
          { "visibility": "on" }
        ]
      },{
        "featureType": "landscape",
        "stylers": [
          { "visibility": "on" },
          { "color": "#f3f4f0" }
        ]
      },{
        "featureType": "poi",
        "stylers": [
          { "visibility": "on" }
        ]
      },{
        "featureType": "road",
        "stylers": [
          { "visibility": "on" }
        ]
      },{
        "featureType": "transit",
        "stylers": [
          { "visibility": "on" }
        ]
      },{
        "featureType": "water",
        "stylers": [
          { "color": "#ccdea3" }
        ]
      }
    ] 

    map.setOptions({
        styles: styles
    });
}

function reSizeMap(){
    //console.log("reSizeMap");

    var mapOptions = {
        center: myLatlng,
    };

    map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);
}
// Initialise the Map
