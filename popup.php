<?php
@session_start();
require 'include/rand.php';
 $catcha_invalid = false;
$_SESSION['captcha_id'] = $str;
?>
<link href="css/pop_style.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.validate.js" type="text/javascript"></script>
<style type="text/css">
.errors {
	color: #900;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
input.errors {
	border: 1px solid #900;
}
</style>
<script type="text/javascript">
  $(document).ready(function() {
         $("#popupform").validate({
           errorClass:"errors", 
           errorElement:"span",
             rules: {
                  captcha: {
                            required: true,
                            remote: "include/process.php"
                           },
                  },
            messages: {
                  name:{
                            required:"Name is required."                
                        
                        },                                             
                   phone:
                   {

                        required:"Phone no is required.",
                        digits:"Only digits in phone no."                
                    },              
                    email:
                    {
                        required:"Email id is required.",
                        email:"Please, enter a valid email address."
                    },      
                     website:{
                            required:"Website is required.",                
                            url:"Please, enter a valid website."
                        },              
                    captcha:
                    {
                        required:"Captcha is required.",
                        remote:"Captcha code incorrect !"
                    }
      
                }
            });
        });
</script>
</head><body>
<div class="popup_bg">
  <div class="popup-close"><a href="javascript:;" title="close"></a></div>
  <div class="ewd_background">
    <h1 class="free-analysis-title"></h1>
    <h2>Receive a Comprehensive 20 Page Analysis Of Your Website.</h2>
    <div class="free-analysis-content">
      <div class="left">
        <h3>This Report Will Break Down Any Errors That Could Be Harming Your Ranking and Conversions. </h3>
          <ul class="ewd_identify">
            <li>Identify Coding Errors</li>
            <li>Identify Keyword info</li>
            <li>Instant Report</li>
          </ul>
      </div>
      <div class="right">
        <h4> Where Should We Email Your Website Review </h4>
        <form method="post" action="popupmail" id="popupform" name="popupform">
          <input type="hidden" name="request_uri" value="<?= $_SERVER['REQUEST_URI'] ?>">
          <p>
            <input type="text" placeholder="Name"  name="name" class="required"/>
          </p>
          <p>
            <input type="text" placeholder="Email" name="email" class="required email" />
          </p>
          <p>
            <input type="text" placeholder="Website" name="website" class="required url website"/>
          </p>
          <!--<p>
                <input type="text" placeholder="Phone" name="phone" class="required digits"/>
              </p>-->
          <input type="submit" name="submit" value="Review" class="review"/>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(window).ready(function(){
        $(".popup-close").click(function(){
            $.fancybox.close();
        });
    });
</script>
<?php /*?><img src="include/captcha/image.php?<?php// echo time(); ?>" alt="captcha.jpg"/>
              <input type="text" placeholder="Enter Captcha Code here..." name="captcha" class="required captcha"/>
                <?php if($catcha_invalid) { ?>
                <span for="captcha" generated="true" class="error">Captcha is required.</span>
                <?php } ?><?php */?>
