<?php include('include/site.php'); ?>
<title>etraffic Web Marketing</title>
</head>
<body>
<?php include('include/header.php'); ?>
<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content inner">
            <h1>Case studies</h1> 	
            <h3 class="post-title"><a href="blog-detail.php">New feature of Google analytics – “Change History”</a></h3>
            <div class="post-meta">
                    <span class="date">January 17, 2013</span>
                    <span class="author-name">Cameron Francis</span>
                    <span class="comments">Case Study</span>
                </div>
            <p>Google analytics as we all know is software that is very commonly being used by a number of site administrators to view the kind of traffic and hits that their website gets. It is software that can be used to monitor traffic to our website and can be used to make changes in the filters goals etc. Google has recently announced a <a href="#">new feature in Google analytics</a> that will help administrator to see that by whom and when the changes has made in the account.</p>
            <div class="post-thumb">
                <a href="#"><img src="images/blog_inner_img.jpg" width="619" height="250" alt="post-img" class="aligncenter"></a>
            </div>
            <div class="post-content">
                <h3>Why Do We Need “Change History” Feature:</h3>
                <p>Though everyone can have access to our website, to change certain key features an admin access is required. Sometimes it so happens that for a large website there are a group of people who have admin access to Google analytics and this leads to a lot of confusion that arises when one administrator makes some change to the website and it’s not properly documented hence resulting in the other administrators not knowing about it. To avoid this confusion <strong>Google analytics has introduced a new feature that is available to site administrators known as “change history”.</strong> This feature displays all the changes that have been made to the website in the last 180 days. It also keeps a record of who made changes to the website and documents all of this in a log that can be viewed by all site administrators.</p>
                <p>The change history specifically lists three columns, the date column where the date and time of the change is displayed. The email column, displaying which analytics user made the change. And the activity column that specifies which analytic object has been modified.</p>
                <h3>How to use: </h3>
                <p>For all existing users of Google analytics, <strong>this option should be visible under the admin settings on the admin tab.</strong> To find the option, we need to navigate to the admin tab on the screen. On the buttons available on the top, we need to click the link for the account which will open the account page. <strong>On the account page once we click the “change history” tab all the changes made to our account in the last 180 days will be visible.</strong> Currently this feature is available to view the history for the last 180 days only but it should get extended to more than that soon.  Visit the <a href="#">Google analytics</a> help center article for more details.</p>
                <h3>Key Features: </h3>
                <p>The key features of this option are that it automatically logs all changes that are made to the account. This reduces a lot of effort that goes into tracking the changes for <a href="#">SEO experts.</a> Also there need not be some dedicated personnel who should be appointed especially to document the changes made to the account as and when they are made. Also when any of the administrator opens the analytics page, he will not get confused about who made changes and why. Also he can keep track of the changes that he himself made to the site and may have forgotten.</p>
                <p>This feature was to come in a long time ago, and it is a good thing that Google has finally rolled it in for its current users to make use of. The option is very simple and easy to use, thought there is no news about whether the change history logs can be exported for offline viewing but it would be an expected feature soon.</p>
            </div>
            <div class="addthis_toolbox"><a href="#"><img src="images/post-share.png" width="310" height="26" alt="share"></a></div>
            <div id="comments">
                <div id="respond">
                    <h3>Comment here</h3>
                    <div class="cancel-comment-reply"><small>Please show respect to all users<a href="#">Cancle</a></small></div>
                    <form id="commentform" action="#">
                      <p class="author"><input type="text" id="author" placeholder="Name"></p>
                      <p class="email"><input type="text" id="email" placeholder="email"></p>
                      <p class="url"><input type="text" id="url" placeholder="Website"></p>
                      <p class="comment"><textarea id="comment" placeholder="Message"></textarea></p>
                      <p class="captcha"><img src="images/comment-captcha.jpg" width="219" height="37" alt="captcha"></p>
                      <p class="captcha-code"><input type="text" id="captcha-code" placeholder="Enter Code here"></p>
                      <p class="submit"><input type="submit" id="submit" value="Submit"></p>
                    </form>
                </div>
                <h3 id="comments-title">20 Comment</h3>
                <ol class="commentlist">
                    <li>
                        <div class="comment-author-meta">
                            <div class="comment-author">Amy D. Nelson</div>
                            <div class="comment-meta">Friday, March 17 2013</div>
                        </div>
                        <div class="comment-body">
                        <p>Colon hydrotherapy is not a cure-all, but can assist the body with healing a variety of conditions: constipation, diarrhea, irritable bowel, bloating, excessive gas, indigestion, allergies, candida overgrowth, skin problems, brittle nails and hair, abnormal body odour, unpleasant breath</p></div>
                    </li>
                    <li>
                        <div class="comment-author-meta">
                            <div class="comment-author">Amy D. Nelson</div>
                            <div class="comment-meta">Friday, March 17 2013</div>
                        </div>
                        <div class="comment-body">
                        <p>Colon hydrotherapy is not a cure-all, but can assist the body with healing a variety of conditions: constipation, diarrhea, irritable bowel, bloating, excessive gas, indigestion, allergies, candida overgrowth, skin problems, brittle nails and hair, abnormal body odour, unpleasant breath</p></div>
                    </li>
                    <li>
                        <div class="comment-author-meta">
                            <div class="comment-author">Amy D. Nelson</div>
                            <div class="comment-meta">Friday, March 17 2013</div>
                        </div>
                        <div class="comment-body">
                        <p>Colon hydrotherapy is not a cure-all, but can assist the body with healing a variety of conditions: constipation, diarrhea, irritable bowel, bloating, excessive gas, indigestion, allergies, candida overgrowth, skin problems, brittle nails and hair, abnormal body odour, unpleasant breath</p></div>
                    </li>
                    <li><a href="#" class="view-all">View All Comments</a></li>
                </ol>
            </div>
          </div>
        </article>
        <aside class="widget-area">
          <div class="widget_categories">
             <h3 class="widget-title">Categories</h3>
             <div class="widget">
             <ul>
                <li><a href="#">Blog Post</a></li>
                <li><a href="#">Case Studies</a></li>
                <li><a href="#">Infographs</a></li>
             </ul>
             </div>
          </div>
          <div class="widget_recent_post">
             <h3 class="widget-title">Popular Post</h3>
             <div class="widget">
             <ul>
                <li><a href="#">How to dominate your marketing...</a></li>
                <li><a href="#">Tips on how to create your own...</a></li>
                <li><a href="#">New feature of Google analytics...</a></li>
                <li><a href="#">Social Media Optimisation – The...</a></li>
                <li><a href="#">SEO v/s PPC : Whom To Rely...</a></li>
                <li><a href="#">Indispensable SEO Technique...</a></li>
                <li><a href="#">Web Marketing on Local...</a></li>
             </ul>
             </div>
          </div>
          <div class="widget_subscribe">
             <h3 class="widget-title">Subscribe</h3>
             <div class="widget-wrap">
             <div class="widget">
              <p>Get email about next update</p>
              <form action="#" class="subscribe">
                <input type="text" palceholder="Your email here">
                <input type="submit" value="">
              </form>
             </div>
             </div>
          </div>
        </aside>
    </div>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>
<!--<div class="pagination">
                <a class="prev" href="#">Prev</a>
                <a class="page-numbers" href="#">1</a>
                <span class="current">2</span>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">...</a>
                <a class="page-numbers" href="#">12</a>
                <a class="next" href="#">Next</a>
                <div class="clear"></div>
            </div>-->