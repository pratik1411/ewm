<!-- Recent Blog start -->
<section class="hrecent-blog">
	<div class="wrapper">
		<div class="left">
        	<h2>Recent Blog Post</h2>
            <ul class="recent-blog-left"> 
   <?php
            include('./blog/wp-blog-header.php');
            ?>
            
            <?php
            $posts = get_posts('numberposts=6&order=DESC');
            foreach ($posts as $post) : start_wp(); ?>
            <li>
		
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo substr(the_title('', '', FALSE), 0, 30); ?>..</a>
            
             </li>
            <?php
            endforeach;
            ?>
                
            </ul>
        </div>
        <div class="right">
        	<ul class="recent-blog-right">
 <?php
            //$posts = get_posts('numberposts=2&order=DESC');
            $posts = $wpdb->get_results("SELECT * FROM wp_posts where post_status ='publish' ORDER BY post_date DESC LIMIT 0 , 2" );
            //var_dump($lastid);

            foreach ($posts as $post) : start_wp(); ?>
            <li>
<?php
 if ( has_post_thumbnail()): 
    the_post_thumbnail('custom-size1');  
endif;
?>
<?php echo get_avatar( $comment, 60); ?>
<!-- <img src="images/blog.jpg"  class="avtar"> -->
<span class="date"><?php the_time('F j,Y'); ?></span>
           <h2 class="title"> <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
         <?php the_excerpt(); ?>
             </li>
            <?php
            endforeach;
            ?>

            	
            </ul>
        </div>
        <div class="c"></div>
    </div>    
</section>
<!-- Recent Blog close -->
