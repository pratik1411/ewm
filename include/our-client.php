<!-- Our Client start -->
<section class="ourclient slider">
	<div class="wrapper">
    <h2 class="box-title"><span>Our Clients</span></h2>    
    <a class="prev">prev</a>            
    <div class="scrollable">                
        <div class="items">
            <div class="data"><img src="images/our-client-1.png" alt="Our-client"></div>  
            <div class="data"><img src="images/our-client-2.png" alt="Our-client"></div>  
            <div class="data"><img src="images/our-client-3.png" alt="Our-client"></div>  
            <div class="data"><img src="images/our-client-4.png" alt="Our-client"></div>  
        </div>                
    </div>                         
    <a class="next">next</a>
    <div class="navi"></div>
      
    </div>    
    <div class="c"></div>
</section>
<!-- Our Client close -->