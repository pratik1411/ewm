<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/base.css" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="css/jquery.css" rel="stylesheet" type="text/css">
<link href="css/example1.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/pop_style.css">

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.js"></script>
<link href="css/jquery.multiselect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.multiselect.js"></script>


<script type="text/javascript" src="js/scrollable.js"></script>

<script src="js/timeline.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/timeline.css" />
<script type="text/javascript" src="js/jquery_002.js"></script>
<script type="text/javascript">	
$(document).ready(function() { 
$('.banner').oneByOne({
	className: 'oneByOne1',	             
	easeType: 'random',
	slideShow: true
});  
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	var getTimelineData = function() {
		var data = [];

		$('#timeline_data > div').each(function(i, el) {
			data.push({
					type:     'blog_post',
					date:     $(el).data('date'),
					title:    $(el).find('h2').text(),
					width:    477,
					content:  $(el).find('div').first().html()
				});
		});
			
		return data;
	};

	var timeline = new Timeline($('#timeline'), getTimelineData());
	timeline.setOptions({
		animation:   true,
		lightbox:    true,
		showYear:    false,
		allowDelete: false,
		columnMode:  'dual',
		order:       'desc'
	});
	timeline.display();
	
	$('#timeline .column_right .timeline_element:even').addClass("red").append( "<div class='arrow-dot'></div>" );
	$('#timeline .column_left .timeline_element:even').addClass("yellow").append( "<div class='arrow-dot'></div>" );
	$('#timeline .column_left .timeline_element:odd').addClass("black").append( "<div class='arrow-dot'></div>" );	
	$('#timeline .column_right .timeline_element:odd').addClass("green").append( "<div class='arrow-dot'></div>" );
	
	
	
	// menu click
	$(document).on('click', '#menu > div', function(e) {
		var year      = $(this).text();
		var scroll_to = year == 2013 ? '#timeline' : '#timeline_date_separator_' + year;

		$.scrollTo(scroll_to, 500);
	});

	// load more click
	var year = 2013;
	$('#loadmore').on('click', function(e) {
		var button = $(this);

		if (button.hasClass('loading')) {
			return;
		}

		year--;
		button.addClass('loading').text('Loading...');
		setTimeout(function() {
			var scroll_to = '#timeline_date_separator_' + year;

			button.removeClass('loading').text('Load More');
			$('<div>').text(year).appendTo($('#menu'));

			var new_data = getTimelineData([year]);
			timeline.appendData(new_data);
			$.scrollTo(scroll_to, 500);
		}, 1000);
	});
});
</script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx: 'fade',
		next:   '#next', 
    	prev:   '#prev',
		pager:  '#nav' ,
		timeout:6000
	});
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$.validator.addMethod("NumbersOnly", function(value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");
	
    var validator = $("#ContactIndexForm").validate({
        errorElement: "p",
		rules: {
		captcha: {
		required: true,
		remote: "include/process.php"
	},
	},
	messages: {
		captcha: "Captcha Code Incorrect !",
},
}); });
 </script>
<script type="text/javascript">
$(document).ready(function() {
	$.validator.addMethod("NumbersOnly", function(value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");
	
    var validator = $("#analysIndexForm").validate({
        errorElement: "span",
		rules: {
		captcha: {
		required: true,
		remote: "include/process.php"
	},
	},
	messages: {
		captcha: "Captcha Code Incorrect !",
},
}); });
 </script>
<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
    $(document).ready(function() {
			$(".single").fancybox({
        });
		
		$(".call").fancybox({
				'showCloseButton'	: false,
				'scrolling'		: true,
				'autoDimensions': true,
				'width'    		: 1000,
				'height'   		: 930,
        });

        $("a[rel=example_group]").fancybox({
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'titlePosition' 	: 'over',
            'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });

        $(".various").fancybox({
            'titlePosition'		: 'inside',
            'transitionIn'		: 'none',
            'transitionOut'		: 'none'
        });
	$(".youtube").click(function(e) {
    $.fancybox({
            'padding' : 0,
            'type' : 'swf',
            'href' : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'swf' : { 'wmode' : 'transparent', 'allowfullscreen' : 'true' }
        });
    e.preventDefault();
});
		
    });
	
</script>
<script type="text/javascript">
$(document).ready(function(){
	$( "ul.menu li.ui-menu-item ul.ui-menu" ).append( "<span class='arrow'></span>" );
});
</script>

<script type="text/javascript" src="js/setting.js"></script>

<script type="text/javascript">
$(document).ready(function() {	
	
	$(".scrollable").scrollable({ circular: true, initialIndex: 0})
	.navigator({idPrefix: 'a', history: true}).autoscroll({interval: 10000}).data("scrollable");

	$('.scrollable').each(function(i, el) {
		var sl = $(el).data('scrollable');
		var ind = parseInt( Math.random() * sl.getSize() );		
		sl.seekTo(ind);		
	}); 

});

</script>


<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="js/map.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
// GOOGLE MAP INIT
if($('#googleMap').size() > 0)
{
var _myLatlng = new google.maps.LatLng(-37.816777,144.95712);
var _color = "#2591CF";
google.maps.event.addDomListener(window, 'load', initialize(_myLatlng, _color));
google.maps.event.addDomListener(window, 'resize', function() {
	map.setCenter(myLatlng);
});
}
});
</script>
<script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36114159-1']);
   _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


