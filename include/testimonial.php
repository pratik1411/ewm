<!-- Testimonial start -->
<section class="htestimonial">
	<div class="wrapper">
     <h2 class="box-title"><span><img src="images/quatation-mark.png" alt="Testimonial"></span></h2>    
      <ul>
      	 <li>
         	<div class="testi-avtar"><img src="images/testimonial.jpg" alt="testimonial" class="alignleft"></div>
            <div class="detail">
         	<p>The eTraffic Group came into my web world and rocked it! I love my new website. I have had more people join than ever before and they let me put as many airbrushed photos of myself on there as I wanted. They can do anything! "</p>
            <span class="author-name">Mark Wilson</span>
            <span class="post">Sure Seal Bathrooms</span>
            </div>
         </li>
         <li>
         	<div class="testi-avtar"><img src="images/katrena-roordink.jpg" alt="testimonial" class="alignleft"></div>
            <div class="detail">
         	<p>eTraffic elevated the look and feel of my website to a whole new level, resulting in longer visits and increased sales. The eTraffic Group team stayed with us through every tech challenge. We had Flash, video, shopping cart issues, you name it. eTraffic made us the heroes.</p>
            <span class="author-name">Katrena Roordink</span>
            <span class="post">Williamstown Plaster</span>
            </div>
         </li>
      </ul>
    </div>        
</section>
<!-- Testimonial close -->