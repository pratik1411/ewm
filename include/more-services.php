<section class="more-services">
<h2>More Services</h2>
<div class="wrapper">        
<div class="more-services-wrap">
    <ul>
        <li><a href="seo"<?php if($self=='seo.php'){?> class="active"<?php }?>>Search Engine Optimisation</a></li>
        <li><a href="ppc-advertising"<?php if($self=='ppc-advertising.php'){?> class="active"<?php }?>>PPC Management</a></li>
        <li><a href="search-engine-marketing"<?php if($self=='search-engine-marketing.php'){?> class="active"<?php }?>>Search Engine Marketing</a></li>
        <li><a href="social-media-optimisation"<?php if($self=='social-media-optimisation.php'){?> class="active"<?php }?>>Social Media Optimisation</a></li>
        <li><a href="reputation-management"<?php if($self=='reputation-management.php'){?> class="active"<?php }?>>Reputation Management</a></li>
        <li><a href="video-optimisation"<?php if($self=='video-optimisation.php'){?> class="active"<?php }?>>Video Optimisation/Production</a></li>
        <li><a href="local-seo"<?php if($self=='local-seo.php'){?> class="active"<?php }?>>Local SEO</a></li>
        <li><a href="seo-link-building"<?php if($self=='seo-link-building.php'){?> class="active"<?php }?>>SEO Link building</a></li>
        <li><a href="ecommerce-seo"<?php if($self=='ecommerce-seo.php'){?> class="active"<?php }?>>eCommerce SEO</a></li>
        
    </ul>
</div>
</div>
</section>