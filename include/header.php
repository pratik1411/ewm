<script type="text/javascript" src="//s3.amazonaws.com/scripts.hellobar.com/dbb80c9c8a27c45f28a3eb18c2c05aba552913c0.js"></script>
<?php
$self= explode('/',$_SERVER['PHP_SELF']);
$self = $self[ count($self) - 1 ];
?>
<!-- header start -->
<header>
	<div class="wrapper">
	    <a href="#" class="click-to-call"></a>
    	<span class="logo"><a href="index" title="eTraffic Web Marketing"><img src="images/etrafficwebmarketing-logo.png" alt="etraffic-web-marketing-logo"></a></span>
        <nav>
        	<ul class="menu" id="menu">
            	<li><a href="index"<?php if($self=='index'){?> class="active"<?php }?>><span>Home</span></a></li>
                <li><a href="about-us"<?php if($self=='about-us'){?> class="active"<?php }?>><span>About Us</span></a></li>
                <li><a href="our-services"<?php if($self=='services'){?> class="active"<?php }?>><span>Our Services</span></a>
					<ul>
                        <li><a href="seo" class="seo"><span class="ui-icon ui-icon-seo"></span>Search Engine Optimisation</a></li>
                        <li><a href="ppc-advertising" class="ppc"><span class="ui-icon ui-icon-ppc"></span>PPC Management</a></li>
                        <li><a href="search-engine-marketing" class="sem"><span class="ui-icon ui-icon-sem"></span>Search Engine Marketing</a></li>
                        <li><a href="social-media-optimisation" class="smo"><span class="ui-icon ui-icon-smo"></span>Social Media Optimisation</a></li>
                        <li><a href="reputation-management" class="rm"><span class="ui-icon ui-icon-rm"></span>Reputation Management</a></li>
                        <li><a href="video-optimisation" class="vo"><span class="ui-icon ui-icon-vo"></span>Video Optimisation/Production</a></li>
                        <li><a href="local-seo" class="ls"><span class="ui-icon ui-icon-ls"></span>Local SEO</a></li>
                        <li><a href="seo-link-building" class="slb"><span class="ui-icon ui-icon-slb"></span>SEO Link building</a></li>
                        <li><a href="ecommerce-seo" class="es"><span class="ui-icon ui-icon-es"></span>eCommerce SEO</a></li>
                    </ul> 				
				</li>
                <li><a href="blog/"><span>Resource</span></a>
					<ul class="small">
                    	<li><a href="blog/" class="blog"><span class="ui-icon ui-icon-blog"></span>Blog</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/etaffic-tv/" class="tv"><span class="ui-icon ui-icon-tv"></span>etraffic TV</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/social-media/" class="tv"><span class="ui-icon ui-icon-media"></span>Social Media</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/case-studies/" class="case"><span class="ui-icon ui-icon-case"></span>Case Studies</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/infographics/" class="graph"><span class="ui-icon ui-icon-graph"></span>Infographics</a></li>
						<li><a href="http://www.etrafficwebmarketing.com.au/blog/google-webmaster-tools-training/" class="tool"><span class="ui-icon ui-icon-tool"></span>Google Webmaster Tools Training</a></li>												
                    </ul>					
				</li>
                <li><a href="contact-us"<?php if($self=='contact-us'){?> class="active"<?php }?>><span>Contact Us</span></a></li>
            </ul>
        </nav>
        <div class="c"></div>       
    </div>
        </header>
<!-- header close -->
