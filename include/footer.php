<!-- footer start -->
<footer class="grey">
	<div class="shadow-top"><img src="images/shadow-top.png" alt="shadow-top"></div>
	<div class="wrapper">

    	<a href="#login" class="get-a-quote-btn single">Get A Free Website Analysis	</a>
    	<div class="quick-links">
        <h3>Quick Links</h3>
        <ul>
            <li><a href="index">Home</a></li>
            <li><a href="about-us">About Us</a></li>
            <li><a href="our-services">Our Services</a></li>
            <!-- <li><a href="#">Our Packages</a></li> -->
            <li><a href="blog/">Blog</a></li>
            <li><a href="contact-us">Contact Us</a></li>
        </ul>
        </div>
        <div class="our-services">
        	<h3>Our services</h3>
        	<ul>
                <li><a href="seo" class="active">Search Engine Optimisation</a></li>
        		<li><a href="search-engine-marketing">Search Engine Marketing</a></li>
        		<li><a href="social-media-optimisation">Social Media Optimisation</a></li>
                <li><a href="ecommerce-seo">eCommerce SEO</a></li>
                <li><a href="video-optimisation">Video Optimisation/Production</a></li>
            </ul>
        </div>
        <div class="contacts">
          <h3>Contact us</h3>
          <p>
            Level 1, 530 Little Collins Street <br>
            Melbourne VIC 3000<br>
            <strong>1300 887 151</strong><br>
            <a href="mailto:info@eTrafficWebMarketing.com.au" class="email">info@eTrafficWebMarketing.com.au</a>
          </p>
        </div>
        <div class="follow">
        	<h3>follow us</h3>
        	<ul>
            	<li><a href="https://www.facebook.com/eTrafficGroup" class="facebook">Facebook</a></li>
                <li><a href="https://twitter.com/etrafficgroup" class="twitter">Twitter</a></li>
                <li><a href="http://www.linkedin.com/in/cameronfrancis" class="in">Linkedin</a></li>
                <li><a href="http://www.youtube.com/user/MrCameronFrancis" class="youtube">Youtube</a></li>
                <li><a href="http://www.yelp.com.au/biz/the-etraffic-group-melbourne-2" class="yelp">Yelp</a></li>
                <li><a href="https://plus.google.com/111067911665788661802/posts" class="gplus">Google +</a></li>
                <li><a href="http://www.etrafficwebmarketing.com.au/blog/feed/" class="rss">RSS</a></li>
            </ul>
        </div>
		<div class="adward">
			<a href="https://www.google.com/partners/#a_profile;idtf=012216886387804875506" target="_blank">
			<img src="images/google-adwords.png" alt="google-adwords" />
			</a>
		</div>
        <div class="c"></div>        
    </div>    
    <div class="copy">Copyright &copy; <?php echo date('Y'); ?>. eTraffic Web Marketing. All right reserved</div>
</footer>
<!-- footer close -->
<script type="text/javascript" src="js/placeholder.js"></script>
<?php
@session_start();

// Include the random string file for captcha
require 'include/rand.php';

// Set the session contents
$_SESSION['captcha_id'] = $str;

?>
<?php

// if(isset($_REQUEST['submit']))
// {
// if(!isset($_REQUEST['package']))
// {
// $name = $_REQUEST['name'];
// $phone = $_REQUEST['phone'];
// $email = $_REQUEST['email'];
// $url = $_REQUEST['url'];


// //$request = $_REQUEST['request'];

// $to = 'nicolecross1579@gmail.com';//'info@etrafficwebmarketing.com.au';
// $subject = "You have a request for free website anlysis from eTraffic Web Marketing.";
// $headers = "MIME-Version: 1.0" . "\r\n";
// $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
// $headers .= "Dear Admin," . "<br>";
// $headers .= "You have a request for free website anlysis from eTraffic Web Marketing. The details are as below:" . "<br>";
// // More headers
// $headers .= 'From: <'.$email.'>' . "\r\n";
// $email_message = "Name:" .$name. "<br>";
// $email_message .= "Email: ".$email."<br>";
// $email_message .= "Phone: ".$phone." <br>";
// $email_message .= "Website: ".$url." <br>";
// mail($to,$subject,$email_message,$headers);

// //sent user

// $subject1 = "Thankyou For Contacting eTraffic Web Marketing.com.au";
// $headers1 .= "Dear " .$name. "\r\n";
// $email_message1 = "Thanking you for submitting your information. You will receive your website analysis within 2 working days." . "\r\n\n";

// $email_message1 .= "For more information kindly contact us on  1300 887 151." . "\r\n\n";

// $email_message1 .= "Thanks & Regards,"."\r\n\n";

// $email_message1 .= "eTraffic Web Marketing("."http://www.etrafficwebmarketing.com.au/".")\r\n\n";

// mail($email,$subject1,$email_message1,$headers1);
// $message_send="We have received your message, one of our consultants will contact you within 24-48 hours";
    
// }  
// }


?>
<div style="display:none;">
<div class="get-free-analysic" id="login">
<div class="quick-contact-form">
<span class="deco"></span>
 <?php echo $message_send;?>
<form name="f1" id="analysIndexForm" action="footermail" method="post" class="wpcf7-form">
		<h2>Free Website Analysis</h2>
		<p class="url"><input name="url" type="text" class="required url" value= "http://"placeholder="Website URL"></p>
        <p class="name"><input name="name" type="text" class="required" placeholder="Your Name"></p>		
		<p class="email"><input name="email" type="text" class="required email" placeholder="Your Email"></p>
		<p class="phone"><input name="phone" type="text" class="required NumbersOnly" placeholder="Your Contact Number"></p>
		<p class="captcha">
			<img src="include/captcha/image.php?<?php echo time(); ?>" alt="Captcha image" >
			<input type="text" placeholder="Enter captcha code here" class="required captchacode" name="captcha" id="captcha" />
		</p>
		<input type="submit" name="submit" value="Submit">
</form>
		
	</div>
<div class="fancy-close"><a href="javascript:;" onclick="$.fancybox.close();"></a></div>
</div>

</div>
<!-- begin olark code -->

<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){

f[z]=function(){

(a.s=a.s||[]).push(arguments)};var a=f[z]._={

},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){

f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={

0:+new Date};a.P=function(u){

a.p[u]=new Date-a.p[0]};function s(){

a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){

hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){

return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){

b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{

b.contentWindow[g].open()}catch(w){

c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{

var t=b.contentWindow[g];t.write(p());t.close()}catch(x){

b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({

loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});

/* custom configuration goes here (www.olark.com/documentation) */

olark.identify('9723-788-10-9050');/*]]>*/</script>
<noscript><a href="https://www.olark.com/site/9723-788-10-9050/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>

<!-- end olark code -->

<script type="text/javascript">
   var _mfq = _mfq || [];
   (function() {
       var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
       mf.src = "//cdn.mouseflow.com/projects/a07e9f10-ec69-4807-8d49-851e92267a93.js";
       document.getElementsByTagName("head")[0].appendChild(mf);
   })();
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1002661467;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1002661467/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
