<?php
@session_start();
ob_start(); 
$url = "http://www.etrafficwebmarketing.com.au/";
if(isset($_REQUEST['submit']))
{
if(!isset($_REQUEST['package']))
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Etraffic Web Marketing</title>
</head>
<body topmargin="0" leftmargin="0" style="padding:0; margin:0; background-color:#f7f7f7;">
<table cellpadding="0" cellspacing="0" align="center" width="100%" style="background-color:#f7f7f7; font-family:Arial, Helvetica, sans-serif">
  <tr>
      <td valign="top">
          <table cellpadding="0" cellspacing="0" width="718" align="center" style="background:url(<?= $url ?>/images/top-bg.png); background-repeat:no-repeat; background-position:center top; margin-top:20px;">
              <tr>
                    <td width="18"></td>
                    <td width="598" style="background-color:#ffffff; padding:42px" valign="top">
                      <table cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td height="110" align="center" valign="top"><a href="#"><img src="<?= $url ?>/images/etraffic-logo.jpg" alt="etraffic-logo" /></a></td>
                            </tr>
                            <tr>
                              <td style="background-color:#4b565c; color:#ffffff; font-size:18px; text-transform:uppercase; font-family:Arial, Helvetica, sans-serif; font-weight:bold; padding:10px;">
                                A new lead generated
                                </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td style="color:#000000; font-weight:bold; font-size:14px;">Hi Administrator</td>
                           </tr>
                            <tr>
                              <td style="color:#000000; font-size:14px; padding:15px 0px;">Great News!</td>
                            </tr>
                            <tr>
                              <td style="color:#000000; font-size:14px;">We have a new lead for you</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td valign="top" style="background-color:#f8f8f8; padding:5px">
                                  <table cellpadding="0" cellspacing="0" width="100%" style="background-color:#ffffff; color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif;">
                                      <tr>
                                          <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px">            Name
                                            </td>
                                            <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                              <?= $_POST['name'] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">           Email
                                            </td>
                                            <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px">
                                              <a href="#" style="color:#555555; text-decoration:none"><?= $_POST['email'] ?></a>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">           Phone
                                            </td>
                                            <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px">
                                              <a href="#" style="color:#555555; text-decoration:none"><?= $_POST['phone'] ?></a>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">           Website
                                            </td>
                                            <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px; line-height:18px;">
                                              <?= $_POST['url'] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td width="20%" style="color:#999999; font-size:12px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; border-right:2px solid #f8f8f8; border-bottom:2px solid #f8f8f8; text-transform:uppercase; padding:15px 10px;">           Page URL
                                            </td>
                                            <td style="color:#555555; font-size:14px; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #f8f8f8; padding:15px 10px;">
                                              <a href="#" style="color:#555555; text-decoration:none"><?= "http://"."$_SERVER[HTTP_HOST]"."$_POST[request_uri]" ?></a>
                                            </td>
                                    </table>
                                </td>
                            </tr>                            
                        </table>
                    </td>
                    <td width="18"></td>
                </tr>       
            </table>
        </td>
    </tr>
    <tr>
    <td align="center" style="padding:20px"><a href="#"><img src="<?= $url ?>/images/logo.png" alt="" />
</a></td>
   </tr>

</table>
</body>
</html>
<?php
$to ='info@etrafficwebmarketing.com.au';
$subject = 'Contact Us';
$body = ob_get_clean();

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'Bcc: info@etraffic.com.au' . "\r\n";
$headers .= 'From:  Etraffic Web Marketing<noreply@etrafficwebmarketing.com.au>' . "\r\n";

$result = @mail($to,$subject,$body,$headers);

  @session_start();
  ob_start(); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Etraffic Web Marketing</title>
</head>
<body topmargin="0" leftmargin="0" style="padding:0; margin:0; background-color:#f7f7f7;">
<table cellpadding="0" cellspacing="0" width="100%" align="center" style="background-color:#f7f7f7; font-family:Arial, Helvetica, sans-serif">
    <tr>
        <td valign="top">
            <table cellpadding="0" cellspacing="0" width="718" align="center" style="background:url(<?= $url ?>/images/top-bg.png); background-repeat:no-repeat; background-position:center top; margin-top:20px;">
                <tr>
                    <td width="18"></td>
                    <td width="598" style="background-color:#ffffff; padding:42px" valign="top">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td height="110" align="center" valign="top">
                                    <a href="#">

                                 <img src="<?= $url ?>/images/logo.png" alt="" />

                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px; border-top:2px solid #e7e7e7;">
                                
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">Hi <?= $_POST['name'] ?>,</td>
                           </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="color:#000000; font-size:14px;">Thanks for submitting your details through.</td>
                            </tr>
                             <tr>
                                <td>&nbsp;</td>
                            </tr>
							<tr>
                                <td style="color:#000000; font-size:14px;">You will receive your website analysis as soon as possible.</td>
                            </tr>
                             <tr>
                                <td>&nbsp;</td>
                            </tr>
							<tr>
                                <td style="color:#000000; font-size:14px;">For any enquiries please feel free to contact us on any of the details below</td>
                            </tr>
                             <tr>
                                <td>&nbsp;</td>
                            </tr>                          
							<tr>
                                <td style="color:#000000; font-size:14px; font-weight:bold;">Kind Regards,</td>
                            </tr>
							<tr>
                                <td>&nbsp;</td>
                            </tr> 
                            <tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">eTraffic Web Marketing</td>
                            </tr>
							<tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">1300 887 151</td>
                            </tr>
							<tr>
                                <td>&nbsp;</td>
                            </tr> 
							<tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">Level 1, 530 Little Collins Street</td>
                            </tr>
							<tr>
                                <td style="color:#000000; font-weight:bold; font-size:14px;">Melbourne VIC 3000</td>
                            </tr>
							<tr>
                                <td>&nbsp;</td>
                            </tr> 
                        </table>
                    </td>
                    <td width="18"></td>
                </tr>               
            </table>
        </td>
    </tr>   
</table>
</body>
</html>
<?php
  
    $to2 =$_POST['email'];
    $subject2 = 'Thankyou For Contacting eTraffic Web Marketing';
    $body2 = ob_get_clean();
    // To send HTML mail, the Content-type header must be set
    $headers2  = 'MIME-Version: 1.0' . "\r\n";
    $headers2 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Additional headers
    $headers2 .= 'From: Etraffic Web Marketing<noreply@etrafficwebmarketing.com.au>' . "\r\n";
    

    $res2=mail($to2,$subject2,$body2,$headers2);
    ?>
<?php
  header('location:thankyou.php');
}
}?>