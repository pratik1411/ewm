<?php include('include/site.php'); ?>
<title>etraffic Web Marketing</title>
</head>
<body>
<?php include('include/header.php'); ?>
<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content">
            <h1>Case studies</h1>
            <article>
            	<h3 class="post-title"><a href="blog-detail.php">New feature of Google analytics – “Change History”</a></h3>
                <div class="post-meta">
                    <span class="date">January 17, 2013</span>
                    <span class="author-name">Cameron Francis</span>
                    <span class="comments">Case Study</span>
                </div>
                <div class="post-thumb">
                    <a href="blog-detail.php"><img src="images/post-img.jpg" alt="post"></a>
                </div>
                <div class="post-content">
                   <p>Google analytics as we all know is software that is very commonly being used by a number of site administrators to view the kind of traffic and hits that their website gets. It is software that can be used to monitor traffic to our website and can be used to make changes in the filters goals etc. Google has recently announced a new feature in Google analytics...
Read More</p>
                   <a href="blog-detail.php" class="reamore">Read More</a>
                 </div>                    
            </article>
            <article>
            	<h3 class="post-title"><a href="blog-detail.php">New feature of Google analytics – “Change History”</a></h3>
                <div class="post-meta">
                    <span class="date">January 17, 2013</span>
                    <span class="author-name">Cameron Francis</span>
                    <span class="comments">Case Study</span>
                </div>
                <div class="post-thumb">
                    <a href="blog-detail.php"><img src="images/post-img.jpg" alt="post"></a>
                </div>
                <div class="post-content">
                   <p>Google analytics as we all know is software that is very commonly being used by a number of site administrators to view the kind of traffic and hits that their website gets. It is software that can be used to monitor traffic to our website and can be used to make changes in the filters goals etc. Google has recently announced a new feature in Google analytics...
Read More</p>
                   <a href="blog-detail.php" class="reamore">Read More</a>
                 </div>                    
            </article>
            <article>
            	<h3 class="post-title"><a href="blog-detail.php">New feature of Google analytics – “Change History”</a></h3>
                <div class="post-meta">
                    <span class="date">January 17, 2013</span>
                    <span class="author-name">Cameron Francis</span>
                    <span class="comments">Case Study</span>
                </div>
                <div class="post-thumb">
                    <a href="blog-detail.php"><img src="images/post-img.jpg" alt="post"></a>
                </div>
                <div class="post-content">
                   <p>Google analytics as we all know is software that is very commonly being used by a number of site administrators to view the kind of traffic and hits that their website gets. It is software that can be used to monitor traffic to our website and can be used to make changes in the filters goals etc. Google has recently announced a new feature in Google analytics...
Read More</p>
                   <a href="blog-detail.php" class="reamore">Read More</a>
                 </div>                    
            </article> 
            <!-- pagintaion start -->
            <div class="wp-pagenavi">
                <a href="#" class="previouspostslink">Prev</a>
                <span class="current">1</span>
                <a href="#" class="page">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <a href="#" class="nextpostslink">Next</a>
            </div>
            <!-- pagintaion close -->
          </div>
        </article>
        <aside class="widget-area">
          <div class="widget_categories">
             <h3 class="widget-title">Categories</h3>
             <div class="widget">
             <ul>
                <li><a href="#">Blog Post</a></li>
                <li><a href="#">Case Studies</a></li>
                <li><a href="#">Infographs</a></li>
             </ul>
             </div>
          </div>
          <div class="widget_recent_post">
             <h3 class="widget-title">Popular Post</h3>
             <div class="widget">
             <ul>
                <li><a href="#">How to dominate your marketing...</a></li>
                <li><a href="#">Tips on how to create your own...</a></li>
                <li><a href="#">New feature of Google analytics...</a></li>
                <li><a href="#">Social Media Optimisation – The...</a></li>
                <li><a href="#">SEO v/s PPC : Whom To Rely...</a></li>
                <li><a href="#">Indispensable SEO Technique...</a></li>
                <li><a href="#">Web Marketing on Local...</a></li>
             </ul>
             </div>
          </div>
          <div class="widget_subscribe">
             <h3 class="widget-title">Subscribe</h3>
             <div class="widget-wrap">
             <div class="widget">
              <p>Get email about next update</p>
              <form action="#" class="subscribe">
                <input type="text" palceholder="Your email here">
                <input type="submit" value="">
              </form>
             </div>
             </div>
          </div>
        </aside>
    </div>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>
<!--<div class="pagination">
                <a class="prev" href="#">Prev</a>
                <a class="page-numbers" href="#">1</a>
                <span class="current">2</span>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">...</a>
                <a class="page-numbers" href="#">12</a>
                <a class="next" href="#">Next</a>
                <div class="clear"></div>
            </div>-->