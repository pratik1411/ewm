<?php include('include/site.php'); ?>
<title>etraffic Web Marketing</title>
</head>
<body>
<?php include('include/header.php'); ?>
<section class="inner-page">
<!-- page title start -->
<section class="page-title">
    <div class="wrapper">
        <h1>Social Media Optimisation</h1>    
        <span class="sub-title">Nunc tempus felis vitae urna. Vivamus portti torneq</span>
    </div>
</section>
<!-- page title close -->
    <section class="content">
        <div class="wrapper">
        <img src="images/services-detail.jpg" alt="services-detail" class="alignright">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh. Duis tincidunt lectus quis dui viverra vestibulum.</p>
        <p>Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. Maecenas placerat, nisl at consequat rhoncus, sem nunc gravida justo, quis eleifend arcu velit quis lacus. Morbi magna magna, tincidunt a, mattis non, imperdiet vitae, tellus. Sed odio est, auctor ac, sollicitudin in, consequat vitae, orci. Fusce id felis. Vivamus sollicitudin metus eget eros.</p>
        </div>
    </section>
    <section class="grey slider two-column">
        <div class="shadow-top"><img src="images/shadow-top.png" alt="shadow-top"></div>
        <div class="wrapper">
            <a class="prev">prev</a>            
            <div class="scrollable">    
            <h2>Why choose eTraffic Web Marketing for SMO?</h2>            
            <div class="items">
                <!-- step 1 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
                <!-- step 2 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
                <!-- step 3 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
                <!-- step 4 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
            </div>                
        </div>                         
            <a class="next">next</a>
            <div class="navi"></div>
        </div>
        <div class="shadow-bottom"><img src="images/shadow-bottom.png" alt="shadow-bottom"></div>
    </section>
    <section class="content">
        <div class="wrapper">
        <img src="images/services-detail2.jpg" alt="services-detail" class="alignleft">
        <h2>What is Social Media Optimisation?</h2>
        <p>Social Media Optimisation is recognised as the process of increasing the awareness of a brand, product or event by making an apt use of various social media communities and outlets in order to create explicit publicity through the medium of internet. SMO is the fastest way to avail your identity to the outer world quickly and proficiently. It is a significant component of any successful website.</p>
        <p>In the recent times, SMO facilitates modern effective ways for organisations to interact and provide support and services to their customers. By making use of various social networking platforms such.</p>
        	<div class="why-services">
            <h2>Why Social Media Optimisation services?</h2>
            <div id="vtabs">
                <ul>
                    <li><a href="#tabs-1">1</a></li>
                    <li><a href="#tabs-2">2</a></li>
                    <li><a href="#tabs-3">3</a></li>
                </ul>
                <div id="tabs-1">
                <img src="images/tab-img.jpg" alt="tab" class="alignright">
                <p><strong>1 Raise Brand awareness</strong>
                SMO has got immense importance since it raises a good brand awareness of your business, irrespective of its scale and time of introduction. Various social networking platforms such as Facebook, Google+, Twitter and others prove to contribute a lot to raise brand awareness with SMO. Once you get created your fan pages on these social networking sites a huge fan list for your site pages will be spreading similar to the viral marketing among the all other users of the networking sites. 1Lorem ipsum dolor sit amet,</p>
                <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>
                <div id="tabs-2">
                <img src="images/tab-img.jpg" alt="tab" class="alignright">
                <p><strong>2 Raise Brand awareness</strong>
                SMO has got immense importance since it raises a good brand awareness of your business, irrespective of its scale and time of introduction. Various social networking platforms such as Facebook, Google+, Twitter and others prove to contribute a lot to raise brand awareness with SMO. Once you get created your fan pages on these social networking sites a huge fan list for your site pages will be spreading similar to the viral marketing among the all other users of the networking sites. 1Lorem ipsum dolor sit amet,</p>
                <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
              <div id="tabs-3">
              <img src="images/tab-img.jpg" alt="tab" class="alignright">
              <p><strong>3 Raise Brand awareness</strong>
                SMO has got immense importance since it raises a good brand awareness of your business, irrespective of its scale and time of introduction. Various social networking platforms such as Facebook, Google+, Twitter and others prove to contribute a lot to raise brand awareness with SMO. Once you get created your fan pages on these social networking sites a huge fan list for your site pages will be spreading similar to the viral marketing among the all other users of the networking sites. 1Lorem ipsum dolor sit amet,</p>
                <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
            </div>
        </div>
    </section>    
    <section class="grey slider two-column">
        <div class="shadow-top"><img src="images/shadow-top.png" alt="shadow-top"></div>
        <div class="wrapper">
            <a class="prev">prev</a>            
            <div class="scrollable">    
            <h2>Our strategic approach to social media optimization</h2>            
            <div class="items">
                <!-- step 1 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
                <!-- step 2 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
                <!-- step 3 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
                <!-- step 4 -->
                <div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  
            </div>                
        </div>                         
            <a class="next">next</a>
            <div class="navi"></div>
        </div>
        <div class="shadow-bottom"><img src="images/shadow-bottom.png" alt="shadow-bottom"></div>
    </section>
    <section class="content">
        <div class="wrapper">
    <p>eTraffic Web Marketing has a team of Web Professionals who have years of experience in the field of Web Marketing and Design. Each eTraffic professional is carefully chosen to make sure that we get only the people who have undying passion for the job.</p>
    <p>Below is a list of the various services offered by eTraffic Web Marketing to small and medium businesses who want to maximise the power of the internet.</p>
    <?php include('include/like-work.php'); ?>
     </div>
    </section>
    <?php include('include/more-services.php'); ?>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>