<?php require 'sendmail.php' ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/base.css" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="css/jquery.css" rel="stylesheet" type="text/css">
<link href="css/example1.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.js"></script>
<link href="css/jquery.multiselect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery.multiselect.js"></script>
<script type="text/javascript" src="js/scrollable.js"></script>
<script type="text/javascript" src="js/setting.js"></script>
<script src="js/timeline.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/timeline.css" />
<script type="text/javascript" src="js/jquery_002.js"></script>
<script type="text/javascript">	
$(document).ready(function() { 
$('.banner').oneByOne({
	className: 'oneByOne1',	             
	easeType: 'random',
	slideShow: true


});  
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	var getTimelineData = function() {
		var data = [];

		$('#timeline_data > div').each(function(i, el) {
			data.push({
					type:     'blog_post',
					date:     $(el).data('date'),
					title:    $(el).find('h2').text(),
					width:    477,
					content:  $(el).find('div').first().html()
				});
		});
			
		return data;
	};

	var timeline = new Timeline($('#timeline'), getTimelineData());
	timeline.setOptions({
		animation:   true,
		lightbox:    true,
		showYear:    false,
		allowDelete: false,
		columnMode:  'dual',
		order:       'desc'
	});
	timeline.display();
	
	$('#timeline .column_right .timeline_element:even').addClass("red").append( "<div class='arrow-dot'></div>" );
	$('#timeline .column_left .timeline_element:even').addClass("yellow").append( "<div class='arrow-dot'></div>" );
	$('#timeline .column_left .timeline_element:odd').addClass("black").append( "<div class='arrow-dot'></div>" );	
	$('#timeline .column_right .timeline_element:odd').addClass("green").append( "<div class='arrow-dot'></div>" );
	
	
	
	// menu click
	$(document).on('click', '#menu > div', function(e) {
		var year      = $(this).text();
		var scroll_to = year == 2013 ? '#timeline' : '#timeline_date_separator_' + year;

		$.scrollTo(scroll_to, 500);
	});

	// load more click
	var year = 2013;
	$('#loadmore').on('click', function(e) {
		var button = $(this);

		if (button.hasClass('loading')) {
			return;
		}

		year--;
		button.addClass('loading').text('Loading...');
		setTimeout(function() {
			var scroll_to = '#timeline_date_separator_' + year;

			button.removeClass('loading').text('Load More');
			$('<div>').text(year).appendTo($('#menu'));

			var new_data = getTimelineData([year]);
			timeline.appendData(new_data);
			$.scrollTo(scroll_to, 500);
		}, 1000);
	});
});
</script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
		fx: 'fade',
		next:   '#next', 
    	prev:   '#prev',
		pager:  '#nav' ,
		timeout:6000
	});
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$.validator.addMethod("NumbersOnly", function(value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");
	
    var validator = $("#ContactIndexForm").validate({
        errorElement: "p",
		rules: {
		captcha: {
		required: true,
		remote: "include/process.php"
	},
	},
	messages: {
		captcha: "Captcha Code Incorrect !",
},
}); });
 </script>
<script type="text/javascript">
$(document).ready(function() {
	$.validator.addMethod("NumbersOnly", function(value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");
	
    var validator = $("#analysIndexForm").validate({
        errorElement: "p",
		rules: {
		captcha: {
		required: true,
		remote: "include/process.php"
	},
	},
	messages: {
		captcha: "Captcha Code Incorrect !",
},
}); });
 </script>
<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
    $(document).ready(function() {
			$(".single").fancybox({
        });
		
        $("a[rel=example_group]").fancybox({
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'titlePosition' 	: 'over',
            'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });

        $(".various").fancybox({
            'titlePosition'		: 'inside',
            'transitionIn'		: 'none',
            'transitionOut'		: 'none'
        });
	$(".youtube").click(function(e) {
    $.fancybox({
            'padding' : 0,
            'type' : 'swf',
            'href' : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'swf' : { 'wmode' : 'transparent', 'allowfullscreen' : 'true' }
        });
    e.preventDefault();
});
		
    });
	
</script>
<!-- code here close -->

<title>Quick Contact - eTraffic Web Marketing</title>

</head>
<body>
<?php include('include/header.php'); ?>
<!--<section id="google-map" class="map">
<div id="googleMap" style="width: 100%; height:100%;">map</div>
<div class="contact-info">
    <div class="left">
    	<h1>Contact Us</h1>
        <span class="hello">Say Hello</span>
    </div>
    <div class="right">
        <span class="mail"><a href="mailto:info@eTrafficWebMarketing.com.au">info@eTrafficWebMarketing.com.au</a></span>
        <span class="location">
            <strong>eTraffic Web Marketing</strong><br>
            Level 1, 530 Little Collins Street<br> 
            Melbourne VIC 3000<br>
            Australia<br>
            <strong>1300 887 151</strong>
        </span>
    </div>
</div>
</section>-->
<section class="inner-page">
	<div class="wrapper">
    	<h2>Quick Contact</h2>
        <div class="mssage"><p><strong>Just fill out your information below and we will get in touch with you to arrange the next step forward for your online campaign.</strong></p></div>
        <?php //echo $message_send;?>
        <form name="f1" id="ContactIndexForm" action="#" method="post" class="wpcf7-form">
        	<div class="left">
            	<p><input type="text" name="name" class="required" placeholder="Your Name"></p>
                <p><input type="text" name="email" class="required email" placeholder="Your Email Address"></p>
                <p><input type="text" name="phone" class="required NumbersOnly" placeholder="Your Phone"></p>
                <p class="captcha"><img src="include/captcha/image.php?<?php echo time(); ?>" alt="Captcha image"  width="129" height="50" ><input type="text" placeholder="Enter captcha code here" class="required captchacode" name="captcha" id="captcha" /></p>
            </div>
            <div class="right">
				<p>
			<select name="package[]" multiple="multiple"  class="select" >
            <option selected="select" value="">Marketing Required</option>
		    <option value="Search Engine Optimization">Search Engine Optimization</option>
		    <option value="Social Media Optimization">Social Media Optimization</option>
		    <option value="Search Engine Marketing">Search Engine Marketing</option>
		    <option value="PPC">PPC</option>
		    <option class="Reputation Management">Reputation Management</option>
		    <option value="Content Writing">Content Writing</option>
              </select>
		<!--<select name="package">
	     	    <option>Required Analysis For</option>
		    <option value="Search Engine Optimization">Search Engine Optimization</option>
		    <option value="Social Media Optimization">Social Media Optimization</option>
		    <option value="Search Engine Marketing">Search Engine Marketing</option>
		    <option value="PPC">PPC</option>
		    <option class="Reputation Management">Reputation Management</option>
		    <option value="Content Writing">Content Writing</option>
		    </select> -->
</p>
                <p><textarea name="message"placeholder="Enter Your Message Here"></textarea></p>
                <p><input type="submit" name="submit" value="I'm done, Send"></p>
            </div>
        </form>
        
        
        
    </div>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>
