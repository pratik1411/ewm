<?php
session_start();

// Include the random string file for captcha
require 'include/rand.php';

// Set the session contents
$_SESSION['captcha_id'] = $str;

?>
<?php

if(isset($_REQUEST['submit']))
   {

$name = $_REQUEST['name'];
$phone = $_REQUEST['phone'];
$email = $_REQUEST['email'];
$package = $_REQUEST['package'];


//$commaSeparatedvalues = implode( ",",$_REQUEST['package'] ); 

/*if(empty($interest))
  {
    echo("You didn't select any option.");
  }
else{
 $N = count($interest);
    echo("You selected $N door(s): ");
    for($i=0; $i < $N; $i++)
    {
      echo ($interest[$i] . " ");
    }


}*/


/*foreach ($interest as $c2)   
{  
    echo $c2;  
}*/
/*$interest='';
  for ($i=0; $i<count($_POST['interest']);$i++) {
         $interest .= $_POST['interest'][$i];
         echo $interest;
         $interest =","; 
           
       }*/


 
$message = $_REQUEST['message'];

//$request = $_REQUEST['request'];


$to = 'info@etrafficwebmarketing.com.au';
$subject = "Lead  Contact form of http://www.etrafficwebmarketing.com.au";


$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";



// More headers
$headers .= 'From: <'.$email.'>' . "\r\n";
if(count($package)>0) {
 for($i=0;$i<count($package);$i++){
  $a = "$package[$i] \n";
$des_12 .= $_REQUEST['package'][$i];
         $des_12 .=","; 
  
  }
$a=  $des_12;

 
 }

    $email_message = "Name:" .$name. "<br>";
    $email_message .= "Email: ".$email."<br>";
    $email_message .= "Phone: ".$phone." <br>";
    $email_message .= "package: ".$a." <br>";
    $email_message .= "Message: ".$message."<br>";
    
    mail($to,$subject,$email_message,$headers);

    $message_send="We have received your message, one of our consultants will contact you within 24-48 hours";
    
    
}


?>


<?php include('include/site.php'); ?>
<title>etraffic Web Marketing</title>
<link rel="stylesheet" type="text/css" href="ui/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="ui/demos/assets/style.css" />
<link rel="stylesheet" type="text/css" href="ui/demos/assets/prettify.css" />
<link rel="stylesheet" type="text/css" href="ui/demos/jquery-ui.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="ui/src/jquery.multiselect.js"></script>
<script type="text/javascript" src="ui/demos/assets/prettify.js"></script>
<script type="text/javascript">
$(function(){

    $("select").multiselect({
        selectedList: 4
    });
    
});
</script>
</head>
<body>
<?php include('include/header.php'); ?>
<section id="google-map" class="map">
<div id="googleMap" style="width: 100%; height:100%;">map</div>
<div class="contact-info">
    <div class="left">
    	<h1>Contact Us</h1>
        <span class="hello">Say Hello</span>
    </div>
    <div class="right">
        <span class="mail"><a href="mailto:info@eTrafficWebMarketing.com.au">info@eTrafficWebMarketing.com.au</a></span>
        <span class="location">
            <strong>eTraffic Web Marketing</strong><br>
            Level 1, 530 Little Collins Street<br> 
            Melbourne VIC 3000<br>
            Australia<br>
            <strong>1300 887 151</strong>
        </span>
    </div>
</div>
</section>
<section class="inner-page">
	<div class="wrapper">
    	<h2>Send us a message</h2>
        <p><strong>Just fill out your information below and we will get in touch with you to arrange the next step forward for your online campaign.</strong></p>
        <?php echo $message_send;?>
        <form name="f1" id="ContactIndexForm" action="#" method="post" class="wpcf7-form">
        	<div class="left">
            	<p><input type="text" name="name" class="required" placeholder="Your Name"></p>
                <p><input type="text" name="email" class="required email" placeholder="Your Email Address"></p>
                <p><input type="text" name="phone" class="required NumbersOnly" placeholder="Your Phone"></p>
                <p class="captcha"><img src="include/captcha/image.php?<?php echo time(); ?>" alt="Captcha image"  width="129" height="50" ><input type="text" placeholder="Enter captcha code here" class="required captchacode" name="captcha" id="captcha" /></p>
            </div>
            <div class="right">
				<p><select name="package[]" multiple="multiple"  class="select" >
                    <option selected="select">Subject For Writing</option>
                    <option value="SEO Packages">SEO Packages</option>
                    <option value="On-page Optimisation Packages">On-page Optimisation Packages</option>
                    <option value="Link Building Packages">Link Building Packages</option>
                    <option value="Social Media Marketing Packages">Social Media Marketing Packages</option>
                    <option value="Web Analytics Packages">Web Analytics Packages</option>
                    <option value="Pay Per Click Packages">Pay Per Click Packages</option>
                    <option value="General Enquiry">General Enquiry</option>
                    <option value="Others">Others</option>
              </select></p>
                <p><textarea name="message"></textarea></p>
                <p><input type="submit" name="submit" value="I’m done, Send"></p>
            </div>
        </form>
        <h2 class="box-title"><span>Our Presence</span></h2>
        <p><strong>With over 9 Locations through out Australia - eTraffic Web Marketing can help you no matter where you are...</strong></p>
        <div class="city-list">
        	<ul>
            	<li class="first">
                	<a href="seo-melbourne.php">
                	<span>Melbourne</span>
                    <address>
                    Level 1, 530 Little Collins Street<br>
                    Melbourne VIC 3000<br> 
                    Australia<br>
                    1300 887 151
                    </address>
                    </a>
                </li>
                <li class="second">
                	<a href="seo-sydney.php">
                	<span>Sydney</span>
                    <address>
                    Level 26, 44 Market Street,<br>
                    Sydney, NSW 2000<br> 
            		Australia<br>
					1300 788 679
                    </address>
                    </a>
                </li>
                <li class="last">
                	<a href="seo-hobart.php">
                	<span>Hobart</span>
                    <address>            
                    Level 6 Reserve Bank Building,<br> 
            		111 Macquarie Street,<br>
            		Hobart TAS 7000,<br>
            		Australia<br>
                    1300 885 870
                    </address>
                    </a>
                </li>
              </ul>
              <ul>                
                <li class="first">
                	<a href="seo-perth.php">
                	<span>Perth</span>
                    <address>            
                    Level 18, Central Park<br>
                    152-158 St Georges Terrace
                    Perth, WA 6000 Australia<br>
                    1300 550 753
                    </address>
                    </a>
                </li>
                <li class="second">
                	<a href="seo-adelaide.php">
                	<span>Adelaide</span>
                    <address>
                    Westpac House, Level 24, <br> 
            		91 King William Street <br>
            		Adelaide, SA 5000 Australia<br>
           			1300 669 895
                    </address>
                    </a>
                </li>
                <li class="last">
                	<a href="seo-brisbane.php">
                	<span>Brisbane</span>
                    <address>
                    Level 19, 10 Eagle Street  <br> 
            		Brisbane, QLD 4000 <br>
                    Australia <br>
            		1300 765 709
                    </address>
                    </a>
                </li>
              </ul>
              <ul>
                <li class="first">
                	<a href="seo-canberra.php">
                	<span>Canberra</span>
                    <address>                    
                    Level 1, The Realm, <br> 
            		18 National Circuit, Barton Canberra <br>
                    ACT 2600, Australia<br>
            		1300 765 708
                    </address>
                    </a>
                </li>
                <li class="second">
                	<a href="seo-darwin.php">
                	<span>Darwin</span>
                    <address>            
                    Level 1, Paspalis Centrepoint, <br>
                    48-50 Smith Street	<br> 
            		Darwin, NT 0800 Australia <br>
            		1300 889 815
                    </address>
                    </a>
                </li>
                <li class="last">
                	<a href="seo-gold-coast.php">
                	<span>Gold Coast</span>
                    <address>
                    220 Varsity Parade<br> 
            		Varsity Lakes, QLD 4227 Australia <br>
					1300 887 804
                    </address>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>
