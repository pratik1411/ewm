<?php include('include/site.php'); ?>
<title>etraffic Web Marketing</title>
</head>
<body>
<?php include('include/header.php'); ?>
<section id="google-map" class="map">
<div id="googleMap" style="width: 100%; height:100%;">map</div>
<div class="contact-info">
    <div class="left">
    	<h1>Contact Us</h1>
        <span class="hello">Say Hello</span>
    </div>
    <div class="right">
        <span class="mail"><a href="mailto:info@eTrafficWebMarketing.com.au">info@eTrafficWebMarketing.com.au</a></span>
        <span class="location">
            <strong>etraffic Web Marketing</strong><br>
            Level 1, 530 Little Collins Street<br> 
            Melbourne VIC 3000<br>
            Australia<br>
            <strong>1300 887 151</strong>
        </span>
    </div>
</div>
</section>
<section class="inner-page">
	<div class="wrapper">
    	<h2>eTraffic Web Marketing - SEO Sydney</h2>
        <div class="odd">
        <div class="video right">
        	<img src="images/city-video.jpg" width="460" height="340">
        </div>
        <p>Due to a massive 2012, eTraffic Web Marketing has opened a new location on Market Street in Sydney, Australia specialises in affordable local, national and eCommerce Search Engine Optimisation for small, medium and large businesses.</p>
        <p>Are you looking for a Sydney based SEO company? eTraffic Web Marketing has the expert SEO consultants and tools to fast track your Search Engine Rankings on Google, Yahoo & Bing. In an ever technology crazed world where consumers want information then and there it is as important as ever you position your business for success.</p>
        <strong class="highlight">The Stats That Matter</strong>
        <ul class="style2">
            <li>97% of internet browsers gather information online before they buy</li>
            <li>60% of online searches are suburb or local related</li>
            <li>70% of internet users search online for businesses offline.</li>
        </ul>
        </div>
        <div class="even">
        	<div class="time-table">
            	<table cellpadding="0" cellspacing="0" width="100%">
                	<thead>
                    	<tr>
                        	<td colspan="4" align="center">Opening Hours</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                    	<th>Days</th>
                        <th>Start Time</th>
                        <th>&nbsp;</th>
                        <th>End Time</th>
                    </tr>
                    <tr>
                    	<td>Monday</td>
                        <td>8:30 am</td>
                        <td>to</td>
                        <td>5:30 am</td>
                   </tr>
                   <tr>
                    	<td>Tuesday</td>
                        <td>8:30 am</td>
                        <td>to</td>
                        <td>5:30 am</td>
                   </tr>
                   <tr>
                    	<td>Wednesday</td>
                        <td>8:30 am</td>
                        <td>to</td>
                        <td>5:30 am</td>
                   </tr>
                   <tr>
                    	<td>Thursday</td>
                        <td>8:30 am</td>
                        <td>to</td>
                        <td>5:30 am</td>
                   </tr>
                   <tr>
                    	<td>Friday</td>
                        <td>8:30 am</td>
                        <td>to</td>
                        <td>5:30 am</td>
                   </tr>
                   <tr>
                    	<td>Saturday</td>
                        <td colspan="3" align="center">Closed</td>
                      </tr>
                   <tr>
                    	<td>Sunday</td>
                        <td colspan="3" align="center">Closed</td>
                      </tr>
                   </tbody>
                </table>
            </div>
        	<p><strong>SEO consultants at eTraffic Web Marketing use a variety of methods to ensure you can future proof your business online:</strong></p>
          <ul class="style2">
                <li>Search Engine Optimisation</li>
                <li>Pay Per Click</li>
                <li>Search Engine Marketing</li>
                <li>Content Marketing</li>
                <li>Inbound Marketing Strategies</li>
                <li>Social Media Optimisation</li>
                <li>Reputation Management</li>
                <li>Video Creation and Marketing</li>
            </ul>
        </div>
        <p>Using our diverse specialist SEO services we have helped Sydney start-up businesses develop into successful companies. No matter the industry your company operates in or the size of your company compared to your competitors we have the expertise and the personnel to help you reach your goals.</p>
        <p><strong>Do not delay - a conversation with one of our Marketing Specialists could change your business forever...</strong></p>
        <?php include('include/like-work.php'); ?>
    </div>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>
