<?php
/*
Plugin Name: service_smo
Plugin URI: http://wp.tutsplus.com/
Description: To display post of social media optimisation.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'service_smo1' );
function service_smo1() {
    $labels = array(
        'name' => 'service smo',
        'singular_name' => 'service smo',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New service smo',
        'edit_item' => 'Edit service smo',
        'new_item' => 'New service smo',
        'view_item' => 'View service smo',
        'search_items' => 'Search service smo',
        'not_found' =>  'No service smo found',
        'not_found_in_trash' => 'No service smo in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'service_smo', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 15,
        'supports' => array( 'title','editor','thumbnail')
       
    ) );
}
