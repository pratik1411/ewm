<?php
/*
Plugin Name: SEO
Plugin URI: http://wp.tutsplus.com/
Description: Declares a plugin that will create a custom post type displaying movie reviews.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'create_movie_review' );
function create_movie_review() {
    register_post_type( 'seo',
        array(
            'labels' => array(
                'name' => 'seo',
                'singular_name' => 'seo',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Movie Review',
                'edit' => 'Edit',
                'edit_item' => 'Edit Movie Review',
                'new_item' => 'New Movie Review',
                'view' => 'View',
                'view_item' => 'View Movie Review',
                'search_items' => 'Search Movie Reviews',
                'not_found' => 'No Movie Reviews found',
                'not_found_in_trash' => 'No Movie Reviews found in Trash',
                'parent' => 'Parent Movie Review'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail'),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}
add_action('admin_init','admin_init');
function admin_init(){
  
  add_meta_box("credits_meta", "Design &amp; Build Credits", "credits_meta", "seo", "normal", "low");
}
 
 
function credits_meta() {
  global $post;
  $custom = get_post_custom($post->ID);
  $url = $custom["url"][0];
  return $custom;
  ?>
  <p><label>URl:</label><br />
  <input type="text" name="url" value="<?php echo $url;?>"> 
  <?php
}
add_action('save_post', 'save_details');
function save_details(){
  global $post;
 
  update_post_meta($post->ID, "url", $_POST["url"]);
  
}

?>