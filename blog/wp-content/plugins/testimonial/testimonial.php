<?php
/*
Plugin Name: testimonials
Plugin URI: http://wp.tutsplus.com/
Description: Declares a plugin that will create a custom post type displaying movie reviews.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'testimonials_post_type' );
function testimonials_post_type() {
    $labels = array(
        'name' => 'Testimonials',
        'singular_name' => 'Testimonial',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Testimonial',
        'edit_item' => 'Edit Testimonial',
        'new_item' => 'New Testimonial',
        'view_item' => 'View Testimonial',
        'search_items' => 'Search Testimonials',
        'not_found' =>  'No Testimonials found',
        'not_found_in_trash' => 'No Testimonials in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'testimonials', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 10,
        'supports' => array( 'title','editor','thumbnail')
       
    ) );
}
add_action('admin_init','my_testimonial');
 function my_testimonial()
    {
        add_meta_box("testimonials_meta_boxes", 
            "client_testinomial", 
            "testinomial", 
            "testimonials", 
            "normal", 
            "low"
            );
    }
 function testinomial()
 {
    global $post;
    $custom = get_post_custom($post->ID);
   
    $client_name = $custom['client_name'][0];
    $position = $custom['position'][0];
    ?>
    
    <label>Client Name:</label>
    <input type="text" name="client_name" value="<?php echo $client_name;?>"><br>
    <label>Position</label>
    <input type="text" name="position" value="<?php echo $position;?>">
 <?php 
}
 add_action('save_post', 'save_details1');
function save_details1(){
  global $post;
 
  update_post_meta($post->ID, "testimonial", $_POST["testimonial"]);
  update_post_meta($post->ID, "client_name", $_POST["client_name"]);
  update_post_meta($post->ID, "position", $_POST["position"]);
  
}
?>