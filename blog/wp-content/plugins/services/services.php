<?php
/*
Plugin Name: Our Services
Plugin URI: http://wp.tutsplus.com/
Description: Declares a plugin that will create a custom post type displaying movie reviews.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'our_service' );
function our_service() {
    $labels = array(
        'name' => 'our services',
        'singular_name' => 'our service',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New service',
        'edit_item' => 'Edit service',
        'new_item' => 'New service',
        'view_item' => 'View service',
        'search_items' => 'Search service',
        'not_found' =>  'No service found',
        'not_found_in_trash' => 'No service in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'service', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 10,
        'supports' => array( 'title','editor','thumbnail')
       
    ) );
}

?>