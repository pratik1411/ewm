<?php
/*
Plugin Name: Our-strategic -approach-smo
Plugin URI: http://wp.tutsplus.com/
Description: To display post of social media optimisation.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'strategic_smo' );
function strategic_smo() {
    $labels = array(
        'name' => 'Our-strategic-approach-smo',
        'singular_name' => 'Our-strategic-approach-smo',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New strategic smo',
        'edit_item' => 'Edit strategic smo',
        'new_item' => 'New strategic smo',
        'view_item' => 'View strategic smo',
        'search_items' => 'Search strategic smo',
        'not_found' =>  'No strategic smo found',
        'not_found_in_trash' => 'No strategic smo in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'strategic_approach', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 15,
        'supports' => array( 'title','editor','thumbnail')
       
    ) );
}

?>