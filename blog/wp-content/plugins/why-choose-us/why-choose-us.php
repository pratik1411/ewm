<?php
/*
Plugin Name: Why Choose Us
Plugin URI: http://wp.tutsplus.com/
Description: Declares a plugin that will create a custom post type displaying movie reviews.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'why_choose_us' );
function why_choose_us() {
    register_post_type( 'why_choose_us',
        array(
            'labels' => array(
                'name' => 'why choose',
                'singular_name' => 'why choose',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New why choose',
                'edit' => 'Edit',
                'edit_item' => 'Edit why choose',
                'new_item' => 'New why choose',
                'view' => 'View',
                'view_item' => 'View why choose',
                'search_items' => 'Search why choose',
                'not_found' => 'No why choose found',
                'not_found_in_trash' => 'No why choose found in Trash',
                'parent' => 'Parent why choose'
            ),
 
            'public' => true,
            'menu_position' => 5,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail'),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}

?>