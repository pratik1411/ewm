<?php
/*
Plugin Name: Web Marketing for Smo
Plugin URI: http://wp.tutsplus.com/
Description: Declares a plugin that will create a custom post type displaying movie reviews.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'webmarketing' );
function webmarketing() {
    $labels = array(
        'name' => 'service webmarketing for smo',
        'singular_name' => 'service webmarketing for smo',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New webmarketing',
        'edit_item' => 'Edit webmarketing',
        'new_item' => 'New webmarketing',
        'view_item' => 'View webmarketing',
        'search_items' => 'Search webmarketing',
        'not_found' =>  'No webmarketing found',
        'not_found_in_trash' => 'No webmarketing in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'webmarketing', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 15,
        'supports' => array( 'title','editor','thumbnail')
       
    ) );
}

?>