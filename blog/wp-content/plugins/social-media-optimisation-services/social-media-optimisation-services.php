<?php
/*
Plugin Name: why_smo_service_tabs
Plugin URI: http://wp.tutsplus.com/
Description: To display post of social media optimisation.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/
add_action( 'init', 'service_smo_tab' );
function service_smo_tab() {
    $labels = array(
        'name' => 'why smo service tab',
        'singular_name' => 'why smo service tab',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New service smo',
        'edit_item' => 'Edit service smo',
        'new_item' => 'New service smo',
        'view_item' => 'View service smo',
        'search_items' => 'Search service smo',
        'not_found' =>  'No service smo found',
        'not_found_in_trash' => 'No service smo in the trash',
        'parent_item_colon' => '',
    );
 
    register_post_type( 'why_smo_service', array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 15,
        'supports' => array( 'title','editor','thumbnail')
       
    ) );
}

?>