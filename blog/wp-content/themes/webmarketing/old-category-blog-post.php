<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content">
            <h1>BLOG POST</h1>
            <?php $query = new Wp_Query('post_type=casestudy&order=asc') ?>
            <?php if($query->have_posts()):
               while($query->have_posts()):$query->the_post()?>
            <article>
            	<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                <div class="post-meta">
                    <span class="date"><?php the_time('F j,Y');?></span>
                    <span class="author-name"><?php the_author();?></span>
                    <span class="comments">Blog post</span>
                </div>
                <div class="post-thumb">
                    <a href="blog-detail.php"><?php the_post_thumbnail();?></a>
                </div>
                <div class="post-content">
                   <p><?php the_excerpt();?></p>
                   <!--<a href="blog-detail.php" class="reamore">Read More</a>-->
                 </div>                    
            </article>
        <?php endwhile;?>
    <?php endif;?>
     <?php wp_reset_query() ?>      
            <!-- pagintaion start -->
            <!--<div class="wp-pagenavi">
                <a href="#" class="previouspostslink">Prev</a>
                <span class="current">1</span>
                <a href="#" class="page">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <a href="#" class="nextpostslink">Next</a>
            </div>-->
            <!-- pagintaion close -->
          </div>
        </article>
        <?php get_sidebar();?>
    </div>
</section>

<?php get_footer(); ?>
