<?php
/**
 * Template Name: Casestudy Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
	<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content">
            <h1>Case Study</h1>
            <?php query_posts("post_type=casestudy&posts_per_page=10&order=Desc"); ?>
	            <?php if(have_posts()):
               while(have_posts()):the_post()?>
            <article>
            	<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                <div class="post-meta">
                    <span class="date"><?php the_time('F j,Y');?></span>
                    <span class="author-name"><?php the_author();?></span>
                    <span class="comments"></span>
                </div>
                <div class="post-thumb">
                    <a href="<?php the_permalink();?>"><?php the_post_thumbnail(array(320,213)); ?></a>
                </div>
                <div class="post-content">
                   <p><?php the_excerpt();?></p>
                   <!--<a href="blog-detail.php" class="reamore">Read More</a>-->
                 </div>                    
            </article>
        <?php endwhile;?>
    <?php endif;?>
            <?php wp_pagenavi();?>
          </div>
        </article>
        <?php get_sidebar("casestudy");?>
    </div>
</section>
<?php get_footer(); ?>
