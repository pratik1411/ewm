<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!-- banner start -->
<section class="banner">
	<div class="banner-shadow"></div>
	<div class="wrapper">
	    <a href="#" class="get-a-quote-btn">Get A Quote Now !</a>
		<ul class="slideshow">
            <li>
            <span>Lorem ipsum dolor sit amet, adipiscing elit.</span>
            <p>Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh. Duis tincidunt lectus </p>
            <img src="<?php echo bloginfo('template_directory');?>/images/etraffic-banner1.png" alt="banner1">
            </li>
        </ul>
    </div>
</section>
<!-- banner close -->

<!-- welcome start -->
<section class="welcome">
	<div class="wrapper">
    	<?php echo the_field('short');?>
    </div>   
</section>
<!-- welcome close -->

<!-- search engine optimization start -->
<section class="grey slider">
	<div class="shadow-top"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-top.png" alt="shadow-top"></div>
	<div class="wrapper">    	
        
    <a class="prev">prev</a>            
    <div class="scrollable">                
        <div class="items">
        	<!-- step 1 -->
            
      <?php 
		$loop = new WP_Query(array('post_type' => 'seo', 'posts_per_page' => -1, 'orderby'=> 'Asc')); 
	   		?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
    			<?php global $post;?>
					<div class="data">
      					 <?php the_post_thumbnail('full', array('class' => 'alignleft')); ?>
	 					 <h2><?php the_title();?></h2>
      	 				 <?php the_content();?>
      	 				 <?php $link = get_post_meta($post->ID,'url',true);?>
      	 				 <a href="<?php bloginfo('url');?>/<?php echo $link;?>" class="know-more"><span>Know More</span></a>
   					</div>
			<?php endwhile; ?>

	<?php wp_reset_query(); ?>
               
            
            <!-- step 2 -->
            <!--<div class="data">
            	<img src="<?php echo bloginfo('template_directory');?>/images/search-engine-optimize.png" alt="search-engine-optimization" class="alignleft">
                <h2>Search Engine Optimisation</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>
                <p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. ed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae.</p>
                <a href="#" class="know-more"><span>Know More</span></a>
            </div> -->
            <!-- step 3 -->
            <!--<div class="data">
            	<img src="<?php echo bloginfo('template_directory');?>/images/search-engine-optimize.png" alt="search-engine-optimization" class="alignleft">
                <h2>Search Engine Optimisation</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>
                <p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. ed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae.</p>
                <a href="#" class="know-more"><span>Know More</span></a>
            </div>  -->
            <!-- step 4 -->
            <!--<div class="data">
            	<img src="<?php echo bloginfo('template_directory');?>/images/search-engine-optimize.png" alt="search-engine-optimization" class="alignleft">
                <h2>Search Engine Optimisation</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>
                <p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. ed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae.</p>
                <a href="#" class="know-more"><span>Know More</span></a>
            </div> -->
        </div>                
    </div>                         
    <a class="next">next</a>
    <div class="navi"></div>
        
    </div>
    <div class="c"></div>
    <div class="shadow-bottom"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-bottom.png" alt="shadow-bottom"></div>    
</section>
<!-- search engine optimization close -->

	<!-- etraffic web marketing start -->
<section class="content">
	<div class="wrapper">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php if ( has_post_thumbnail() ) : ?>
					<div class="entry-page-image">
						<?php the_post_thumbnail(); ?>
					</div><!-- .entry-page-image -->
				<?php endif; ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>

		</div>    
</section>
<!-- etraffic web marketing close -->

<!-- why choose start -->
<section class="grey slider">
	<div class="shadow-top"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-top.png" alt="shadow-top"></div>
	<div class="wrapper">
    
    <a class="prev">prev</a>            
    <div class="scrollable">                
        <div class="items">
        	<!-- step 1 -->
        	<?php 
		$why = new WP_Query(array('post_type' => 'why_choose_us', 'posts_per_page' => -1, 'orderby'=> 'Asc')); 
	   		?>
			<?php while ( $why->have_posts() ) : $why->the_post(); ?>
    			<?php global $post;?>
            <div class="data">
            	
            	<?php the_post_thumbnail('full',array('class'=>'alignleft'));?>
                <h2><?php the_title();?></h2>
                <?php the_content();?>
            </div>  
            <?php endwhile; ?>

	<?php wp_reset_query(); ?>
            <!-- step 2 -->
            <!--<div class="data">
            	<img src="<?php echo bloginfo('template_directory');?>/images/why-choose-us.png" alt="why-choose-us" class="alignleft">
                <h2>Why Choose Us?</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>
                <p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. ed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae.</p>
                <p>Nunc tempus felis vitae urna. Vivamus porttitor, neque at volutpat rutrum, purus nisi eleifend libero, a tempus libero lectus feugiat felis. Morbi diam mauris, viverra in, gravida eu.</p>
            </div>  -->
            <!-- step 3 -->
            <!--<div class="data">
            	<img src="<?php echo bloginfo('template_directory');?>/images/why-choose-us.png" alt="why-choose-us" class="alignleft">
                <h2>Why Choose Us?</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>
                <p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. ed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae.</p>
                <p>Nunc tempus felis vitae urna. Vivamus porttitor, neque at volutpat rutrum, purus nisi eleifend libero, a tempus libero lectus feugiat felis. Morbi diam mauris, viverra in, gravida eu.</p>
            </div>  -->
            <!-- step 4 -->
            <!--<div class="data">
            	<img src="<?php echo bloginfo('template_directory');?>/images/why-choose-us.png" alt="why-choose-us" class="alignleft">
                <h2>Why Choose Us?</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam. Praesent mauris ante, elementum et, bibendum at, posuere sit amet, nibh.</p>
                <p>Duis tincidunt lectus quis dui viverra vestibulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue. Aliquam vehicula mi at mauris. ed metus augue, convallis et, vehicula ut, pulvinar eu, ante. Integer orci tellus, tristique vitae.</p>
                <p>Nunc tempus felis vitae urna. Vivamus porttitor, neque at volutpat rutrum, purus nisi eleifend libero, a tempus libero lectus feugiat felis. Morbi diam mauris, viverra in, gravida eu.</p>
            </div> -->
        </div>                
    </div>                         
    <a class="next">next</a>
    <div class="navi"></div>
      
    </div>    
    <div class="c"></div>
    <div class="shadow-bottom"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-bottom.png" alt="shadow-bottom"></div>
</section>
<!-- why choose close -->
<!-- Recent Blog start -->
<section class="hrecent-blog">
	<div class="wrapper">
		<div class="left">
        	<h2>Recent Blog Post</h2>
            <ul class="recent-blog-left">    
                <!--<li><a href="#">How to dominate your marketing...</a></li>
                <li><a href="#">Tips on how to create your own...</a></li>
                <li><a href="#">New feature of Google analytics...</a></li>
                <li><a href="#">Social Media Optimisation – The...</a></li>
                <li><a href="#">SEO v/s PPC : Whom To Rely...</a></li>
                <li><a href="#">Indispensable SEO Technique...</a></li>
                <li><a href="#">Web Marketing on Local...</a></li>-->
                <?php $my_loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 5, 'orderby' => 'id', 'order' => 'DESC' ) );
                 
                  while ($my_loop->have_posts()):$my_loop->the_post();?>
                  	<li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
                  <?php endwhile;?>
            </ul>
        </div>
        <div class="right">
        	<ul class="recent-blog-right">
            <?php $mypost = new WP_Query(array('post_type'=>'post','posts_per_page'=>'2','order'=>'DESC'));
                 while ($mypost->have_posts()):$mypost->the_post();?>
                    <li>
                     <?php the_post_thumbnail();?>
                     <div class="avtar">
                     <?php echo get_avatar( $comment,60 ); ?>
                    </div>
                    
                    <span class="date"><?php the_time('F j, Y');?></span>
                    <h2 class="title"><a href="#"><?php the_title();?></a></h2>
                    <p><?php the_excerpt();?></p>
                    
                </li>
                 <?php endwhile;?>
            	<!--<li>
                	<img src="<?php echo bloginfo('template_directory');?>/images/pic.jpg" alt="blog">
                    <img src="images/blog.jpg"  class="avtar">
                    <span class="date">January 17, 2013</span>
                    <h2 class="title"><a href="#">Lorem ipsum dolor sit amet consectetuer adipiscing elit...</a></h2>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In iaculis facilisis massa. Pellentesque habitant morbi tristique senectus...</p>
                    <a href="#" class="readmore">Read More</a>
                </li>
                <li>
                	<img src="<?php echo bloginfo('template_directory');?>/images/pic.jpg" alt="blog">
                     <img src="images/blog.jpg"  class="avtar">
                    <span class="date">January 17, 2013</span>
                    <h2 class="title"><a href="#">Lorem ipsum dolor sit amet consectetuer adipiscing elit...</a></h2>
                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In iaculis facilisis massa. Pellentesque habitant morbi tristique senectus...</p>
                    <a href="#" class="readmore">Read More</a>
                </li>-->
            </ul>
        </div>
        <div class="c"></div>
    </div>    
</section>
<!-- Recent Blog close -->
<!-- Our Client start -->
<section class="ourclient slider">
	<div class="wrapper">
    <h2 class="box-title"><span>Our Clients</span></h2>    
    <a class="prev">prev</a>            
    <div class="scrollable">                
        <div class="items">
            <div class="data"><img src="<?php echo bloginfo('template_directory');?>/images/our-client.png" alt="client"></div>  
            <div class="data"><img src="<?php echo bloginfo('template_directory');?>/images/our-client.png" alt="client"></div>  
            <div class="data"><img src="<?php echo bloginfo('template_directory');?>/images/our-client.png" alt="client"></div>  
            <div class="data"><img src="<?php echo bloginfo('template_directory');?>/images/our-client.png" alt="client"></div>  
        </div>                
    </div>                         
    <a class="next">next</a>
    <div class="navi"></div>
      
    </div>    
    <div class="c"></div>
</section>
<!-- Our Client close -->
<!-- Testimonial start -->
<section class="htestimonial">
	<div class="wrapper">
     <h2 class="box-title"><span><img src="<?php echo bloginfo('template_directory');?>/images/quatation-mark.png" alt="Testimonial"></span></h2>    
      <ul>
         <?php 
        $testi = new WP_Query(array('post_type' => 'testimonials', 'posts_per_page' => -1, 'orderby'=>'rand')); 
            ?>
            <?php while ( $testi->have_posts() ) : $testi->the_post(); ?>
                <?php global $post;?>
                    <li>
                        
                        <?php the_post_thumbnail('full',array('class'=>'alignleft'));?>
                        <div class="detail">
                            <p><?php the_content();?></p>
                            <span class="author-name"><?php echo get_post_meta($post->ID,'client_name',true);?></span>
                            <span class="post"><?php echo get_post_meta($post->ID,'position',true);?>.</span>
                        </div>
                    </li>
            <?php endwhile; ?>

    <?php wp_reset_query(); ?>
      	 <!--<li>
         	<img src="<?php echo bloginfo('template_directory');?>/images/testimonial.jpg" alt="testimonial" class="alignleft">
            <div class="detail">
         	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam....</p>
            <span class="author-name">Brenna M. Walmsley</span>
            <span class="post">CEO, ABC, Incorp.</span>
            </div>
         </li>
         <li>
         	<img src="<?php echo bloginfo('template_directory');?>/images/testimonial.jpg" alt="testimonial" class="alignleft">
            <div class="detail">
         	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam cursus. Morbi ut mi. Nullam enim leo, egestas id, condimentum at, laoreet mattis, massa. Sed eleifend nonummy diam....</p>
            <span class="author-name">Brenna M. Walmsley</span>
            <span class="post">CEO, ABC, Incorp.</span>
            </div>
         </li>-->
      </ul>
    </div>        
</section>
<!-- Testimonial close -->
<?php get_sidebar( 'front' ); ?>
<?php get_footer(); ?>