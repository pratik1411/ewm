<?php
/**
 * Template Name: service Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<section class="inner-page services">
<section class="page-title">
    <div class="wrapper">
        <h1>Services</h1>    
        <span class="sub-title">Lorem ipsum dolor sit consectet</span><br>
        <span class="sub-title">Morbi ut mi. Null</span>
    </div>
</section>

<section>
    <div class="wrapper">
        <?php echo the_field('short');?>
        <ul class="services-list">
            <?php $service = new WP_Query(array('post_type'=>'service','post_per_page'=>9,'orderby'=>'DESC'));
               while($service->have_posts()):$service->the_post();?>

                  <li>
                    
                    <?php the_post_thumbnail('full');?>
                    <h2><?php the_title();?></h2>
                    <p><?php the_excerpt('30');?></p>
                    
                  </li>

           <?php endwhile; ?>
           <?php wp_reset_query();?>
           <?php flush_rewrite_rules(); ?>
            <!--<li>
                <img src="images/search-engine-optimization.png" alt="search-engine-optimization">
                <h2>Search Engine Optimisation</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            <li>
                <img src="images/search-engine-marketing.png" alt="search-engine-marketing">
                <h2>Search Engine Marketing</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            <li class="last">
                <img src="images/socal-media-optimization.png" alt="socal-media-optimization">
                <h2>Social Media Optimisation</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            
            <li>
                <img src="images/reputation-management.png" alt="reputation-management">
                <h2>Reputation Management</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            <li>
                <img src="images/video-optimization.png" alt="video-optimization">
                <h2>Video Optimisation/Production </h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            <li class="last">
                <img src="images/local-seo.png" alt="local-seo">
                <h2>Local SEO</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            
            <li>
                <img src="images/seo-link-building.png" alt="seo-link-building">
                <h2>SEO Link building</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            <li>
                <img src="images/ecommerce-seo.png" alt="ecommerce-seo">
                <h2>eCommerce SEO</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>
            <li class="last">
                <img src="images/content-marketing.png" alt="content-marketing">
                <h2>Content marketing</h2>
                <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In posuere felis nec tortor. Pellentesque faucibus. Ut accumsan ultricies elit. Maecenas at justo id velit.</p>
                <a href="services-detail.php" class="read-more">Read More</a>
            </li>-->
        </ul>
          <?php the_content();?>
    </div>
</section>
</section>
<?php get_sidebar( 'front' ); ?>
<?php get_footer(); ?>