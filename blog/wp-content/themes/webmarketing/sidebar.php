<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	

	<aside class="widget-area">
          <div class="widget_categories">
             <h3 class="widget-title">BLOG POST</h3>
             <div class="widget">
             <div>
             <ul>
             <?php //dynamic_sidebar( 'sidebar-1' ); ?>
             <?php $args = array(
  'show_option_all'    => '',
  'orderby'            => 'name',
  'order'              => 'ASC',
  'style'              => 'list',
  'show_count'         => 0,
  'hide_empty'         => 1,
  'use_desc_for_title' => 1,
  'child_of'           => 9,
  'feed'               => '',
  'feed_type'          => '',
  'feed_image'         => '',
  'exclude'            => '',
  'exclude_tree'       => '',
  'include'            => '',
  'hierarchical'       => 1,
  'title_li'           => __( '' ),
  'show_option_none'   => __('No categories'),
  'number'             => null,
  'echo'               => 1,
  'depth'              => 0,
  'current_category'   => 0,
  'pad_counts'         => 0,
  'taxonomy'           => 'category',
  'walker'             => null
); ?>

             <?php wp_list_categories($args) ?>
             </ul>
             </div>
             </div>
          </div>
          <div class="widget_recent_post">
             <h3 class="widget-title">Popular Post</h3>
             <div class="widget">
             <!--<ul>
                <li><a href="#">How to dominate your marketing...</a></li>
                <li><a href="#">Tips on how to create your own...</a></li>
                <li><a href="#">New feature of Google analytics...</a></li>
                <li><a href="#">Social Media Optimisation – The...</a></li>
                <li><a href="#">SEO v/s PPC : Whom To Rely...</a></li>
                <li><a href="#">Indispensable SEO Technique...</a></li>
                <li><a href="#">Web Marketing on Local...</a></li>
             </ul>-->
             <?php dynamic_sidebar('sidebar-3');?>
             </div>
          </div>
          <div class="widget_subscribe">
             <h3 class="widget-title">Subscribe</h3>
             <div class="widget-wrap">
             <div class="widget">
              
              <!--<form action="#" class="subscribe">
                <input type="text" palceholder="Your email here">
                <input type="submit" value="">
              </form>-->
              <?php dynamic_sidebar('sidebar-2');?>
             </div>
             </div>
          </div>
        </aside>