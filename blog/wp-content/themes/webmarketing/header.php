<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link href="<?php echo bloginfo('template_directory');?>/css/base.css" type="text/css" rel="stylesheet">
<link href="<?php echo bloginfo('template_directory');?>/css/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/html5.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/scrollable.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/scrollable.navigator.js"></script>
<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/js/setting.js"></script>
<script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-36114159-1']);
   _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php wp_head(); ?>
<a rel="author" href="https://plus.google.com/111253579105134197965/">
<a href="https://plus.google.com/111067911665788661802/" rel="publisher">
</head>

<body>
<!-- header start -->
<header>
	<div class="wrapper">
	    <a href="#" class="click-to-call"></a>
    	<span class="logo"><a href="http://etrafficwebmarketing.com.au" title="etraffic"><img src="<?php echo bloginfo('template_directory');?>/images/etrafficwebmarketing-logo.png" alt="etraffic-web-marketing-logo"></a></span>
        <nav>
        	<ul class="menu" id="menu">
            	<li><a href="http://www.etrafficwebmarketing.com.au"><span>Home</span></a></li>
                <li><a href="http://www.etrafficwebmarketing.com.au/about-us"><span>About Us</span></a></li>
                <li><a href="http://www.etrafficwebmarketing.com.au/our-services"><span>Our Services</span></a>
			<ul>
                        <li><a href="http://www.etrafficwebmarketing.com.au/seo" class="seo"><span class="ui-icon ui-icon-seo"></span>Search Engine Optimisation</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/ppc-advertising" class="ppc"><span class="ui-icon ui-icon-ppc"></span>PPC Management</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/search-engine-marketing" class="sem"><span class="ui-icon ui-icon-sem"></span>Search Engine Marketing</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/social-media-optimisation" class="smo"><span class="ui-icon ui-icon-smo"></span>Social Media Optimisation</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/reputation-management" class="rm"><span class="ui-icon ui-icon-rm"></span>Reputation Management</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/video-optimisation" class="vo"><span class="ui-icon ui-icon-vo"></span>Video Optimisation/Production</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/local-seo" class="ls"><span class="ui-icon ui-icon-ls"></span>Local SEO</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/seo-link-building" class="slb"><span class="ui-icon ui-icon-slb"></span>SEO Link building</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/ecommerce-seo" class="es"><span class="ui-icon ui-icon-es"></span>eCommerce SEO</a></li>
                    </ul> 	
		</li>
               <li><a href="<?php echo get_site_url(); ?>"><span>Resource</span></a>
					<ul class="small">
                    	<li><a href="http://www.etrafficwebmarketing.com.au/blog" class="blog"><span class="ui-icon ui-icon-blog"></span>Blog</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/etaffic-tv/" class="tv"><span class="ui-icon ui-icon-tv"></span>etraffic TV</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/social-media/" class="media"><span class="ui-icon ui-icon-media"></span>Social Media</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/case-studies/" class="case"><span class="ui-icon ui-icon-case"></span>Case Studies</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/infographics/" class="graph"><span class="ui-icon ui-icon-graph"></span>Infographics</a></li>
                        <li><a href="http://www.etrafficwebmarketing.com.au/blog/google-webmaster-tools-training/" class="tool"><span class="ui-icon ui-icon-tool"></span>Google Webmaster Tools Training</a></li>
                    </ul>					
				</li>
                <li><a href="http://www.etrafficwebmarketing.com.au/contact-us"><span>Contact Us</span></a></li>
                <?php //wp_nav_menu(array('menu'=>'nav'));?>
            </ul>
        </nav>
        <div class="c"></div>       
    </div>
</header>
<!-- header close -->
