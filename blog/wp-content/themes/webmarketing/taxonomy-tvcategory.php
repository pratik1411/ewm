<?php
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content inner">
            <?php if ( have_posts() ) : ?>
 <h1><?php printf( __( 'Tv Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>
      <?php while ( have_posts() ) : the_post(); ?>
        <article>
              <h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                <div class="post-meta">
                    <span class="date"><?php the_time('F j,Y');?></span>
                    <span class="author-name"><?php the_author();?></span>
                    <span class="comments"></span>
                </div>
                <div class="post-thumb">
                    <a href="<?php the_permalink();?>"><?php the_post_thumbnail(array(320,213)); ?></a>
                </div>
                <div class="post-content">
                   <p><?php the_excerpt();?></p>
                   <!--<a href="blog-detail.php" class="reamore">Read More</a>-->
                 </div>                    
            </article>
        <?php comments_template( '', true ); ?>
      <?php endwhile; // end of the loop. ?>
    <?php endif; ?>
       </article>
<?php get_sidebar("tv");?>
    </div>
</section>
<?php get_footer(); ?>