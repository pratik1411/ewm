<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<aside class="widget-area">
  <div class="widget_categories">
     <h3 class="widget-title">Webmaster Tools Category</h3>
     <div class="widget">
     <div>
         <?php   $taxonomy = 'webmaster-toolscategory';
          $term_args=array(
            'hide_empty' => false,
            'orderby' => 'name',
            'order' => 'ASC'
          );
          $tax_terms = get_terms($taxonomy,$term_args); ?>
          <ul>
            <?php
              foreach ($tax_terms as $tax_term) {
                echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all %s " ), $tax_term->name ) . '" ' . '>' . $tax_term->name . '' .  '</li>';
              }
            ?>
          </ul>
      </div>
    </div>
          </div>
          <div class="widget_recent_post">
             <h3 class="widget-title">Popular Post</h3>
             <div class="widget">
             <?php query_posts("post_type=webmaster-tools&orderby=comment_count&order=Desc&posts_per_page=10"); ?>
             <ul>
                <?php while ( have_posts() ) : the_post(); ?>
                  <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
             </ul>
            </div>
          </div>
          <div class="widget_subscribe">
             <h3 class="widget-title">Subscribe</h3>
             <div class="widget-wrap">
             <div class="widget">
              <?php dynamic_sidebar('sidebar-2');?>
             </div>
             </div>
          </div>
        </aside>