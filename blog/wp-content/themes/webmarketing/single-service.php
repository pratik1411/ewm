<?php
 /*Template Name: New Template
 */
get_header(); ?>
<section class="inner-page services-detail">
<!-- page title start -->
<section class="page-title">
    <div class="wrapper">
        <h1><?php the_title();?></h1>    
        <span class="sub-title">Nunc tempus felis vitae urna. Vivamus portti torneq</span>
    </div>
</section>
<!-- page title close -->
<section>
        <div class="wrapper">
        <img src="<?php echo bloginfo('template_directory');?>/images/services-detail.jpg" alt="services-detail" class="alignright">

      <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', get_post_format() ); ?>

        
        <?php comments_template( '', true ); ?>

      <?php endwhile; // end of the loop. ?>
      <?php wp_reset_query(); ?>
      </div>
</section>


  <?php
// Find connected weeks
$connected = query_posts( array(
  'connected_type' => 'service_to_webmarketing', // the name of your connection type
  'connected_items' => get_queried_object(),
  'nopaging' => true,
) );?>
  <?php if(!empty($connected)) { ?>
      <section class="grey slider two-column">
        <div class="shadow-top"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-top.png" alt="shadow-top"></div>
        <div class="wrapper">
            <a class="prev">prev</a>            
            <div class="scrollable">    
            <h2>Why choose eTraffic Web Marketing for SMO?</h2>            
            <div class="items">
 
      <?php foreach ( $connected as $post ) {?>
            <?php setup_postdata($post); ?>
        <div class="data">
          <div class="data-detail">
          
           <?php the_content();?>
          </div>
        </div>
     <?php } ?>
   
  
  
  <?php 
  // Prevent weirdness
  wp_reset_postdata();
?>
                <!-- step 1 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  -->
                <!-- step 2 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div> --> 
                <!-- step 3 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div> --> 
                <!-- step 4 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div> --> 
            </div>                
        </div>                         
            <a class="next">next</a>
            <div class="navi"></div>
        </div>
        <div class="shadow-bottom"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-bottom.png" alt="shadow-bottom"></div>
    </section>
<?php } ?>
<?php wp_reset_query();?>
<!--  second slider start from here .............-->
      <?php $connected1 = new WP_Query( array(
  'connected_type' => 'service_to_service_smo', // the name of your connection type
  'connected_items' => get_queried_object(),
  'connected_direction' => 'from',
  'order'=>'DESC',
  'nopaging' => true,
    ) );?>

<section class="content">
    <div class="wrapper">

   <?php if($connected1->have_posts()):
 while ($connected1->have_posts()) {
        $connected1->the_post();?>
       
       <?php the_post_thumbnail('full',array('class'=>'alignleft'));?>
       
      <?php the_content();?>
    
     <?php } ?>
  <?php endif;?>

        <!--<img src="<?php echo bloginfo('template_directory');?>/images/services-detail2.jpg" alt="services-detail" class="alignleft">
        <h2>What is Social Media Optimisation?</h2>
        <p>Social Media Optimisation is recognised as the process of increasing the awareness of a brand, product or event by making an apt use of various social media communities and outlets in order to create explicit publicity through the medium of internet. SMO is the fastest way to avail your identity to the outer world quickly and proficiently. It is a significant component of any successful website.</p>
        <p>In the recent times, SMO facilitates modern effective ways for organisations to interact and provide support and services to their customers. By making use of various social networking platforms such.</p>-->
    </div>
</section>

  <?php wp_reset_query();?>
  <!-- second service slider end here ..........-->

   
           <?php $connected2 = new WP_Query( array(
         'connected_type' => 'service_to_why_smo_service', // the name of your connection type
         'connected_items' => get_queried_object(),
         'connected_direction' => 'from',
         'order'=>'ASC',
         'nopaging' => true,
        ) );?>
    
         <section class="why-services">
          <div class="wrapper">
            <h2>Why Social Media Optimisation services?</h2>
            <div id="vtabs">
                <ul>
                    <?php if($connected2->have_posts()):
                      $i = 1;
              while ($connected2->have_posts()) {
                   $connected2->the_post();?>
                    <li><a href="#tabs-<?php echo $i;?>"><?php echo $i;?></a></li>
                    
                    <?php $i++;?>
                <?php } ?>
              <?php endif;?>
                   
                </ul>
        <?php $i = 1; ?> 
    <?php if($connected2->have_posts()):
          
          while ($connected2->have_posts()) {
            $connected2->the_post();?>
       
        
        <div id="tabs-<?php echo $i;?>">
         <?php the_post_thumbnail('full',array('class'=>'alignright'));?>
         
         <?php the_content();?>
        </div>
        <?php $i++;?>
    
     <?php } ?>
    <?php endif;?>
                <!--<div id="tabs-1">
                <img src="<?php echo bloginfo('template_directory');?>/images/tab-img.jpg" alt="tab" class="alignright">
                <p><strong>1 Raise Brand awareness</strong>
                SMO has got immense importance since it raises a good brand awareness of your business, irrespective of its scale and time of introduction. Various social networking platforms such as Facebook, Google+, Twitter and others prove to contribute a lot to raise brand awareness with SMO. Once you get created your fan pages on these social networking sites a huge fan list for your site pages will be spreading similar to the viral marketing among the all other users of the networking sites. 1Lorem ipsum dolor sit amet,</p>
                <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
               </div>-->
                <!--<div id="tabs-2">
                <img src="<?php echo bloginfo('template_directory');?>/images/tab-img.jpg" alt="tab" class="alignright">
                <p><strong>2 Raise Brand awareness</strong>
                SMO has got immense importance since it raises a good brand awareness of your business, irrespective of its scale and time of introduction. Various social networking platforms such as Facebook, Google+, Twitter and others prove to contribute a lot to raise brand awareness with SMO. Once you get created your fan pages on these social networking sites a huge fan list for your site pages will be spreading similar to the viral marketing among the all other users of the networking sites. 1Lorem ipsum dolor sit amet,</p>
                <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>-->
              <!--<div id="tabs-3">
              <img src="<?php echo bloginfo('template_directory');?>/images/tab-img.jpg" alt="tab" class="alignright">
              <p><strong>3 Raise Brand awareness</strong>
                SMO has got immense importance since it raises a good brand awareness of your business, irrespective of its scale and time of introduction. Various social networking platforms such as Facebook, Google+, Twitter and others prove to contribute a lot to raise brand awareness with SMO. Once you get created your fan pages on these social networking sites a huge fan list for your site pages will be spreading similar to the viral marketing among the all other users of the networking sites. 1Lorem ipsum dolor sit amet,</p>
                <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>-->
            </div>
        </div>
    </section>
  
<!-- slider 4th start from here .............-->

 <?php $connected3 = new WP_Query( array(
         'connected_type' => 'service_to_strategic_approach', // the name of your connection type
         'connected_items' => get_queried_object(),
         'connected_direction' => 'from',
         'order'=>'ASC',
         'nopaging' => true,
        ) );?>
      <?php if(!empty($connected3)) {?>
    <section class="grey slider two-column">
        <div class="shadow-top"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-top.png" alt="shadow-top"></div>
        <div class="wrapper">
            <a class="prev">prev</a>            
            <div class="scrollable">    
            <h2>Our strategic approach to social media optimization</h2>            
            <div class="items">
       
         <?php if($connected3->have_posts()):
          
          while ($connected3->have_posts()) {
            $connected3->the_post();?>
            <div class="data">
             
            <?php the_content();?>
              
            </div>
      <?php } ?>
      <?php endif;?>
                <!-- step 1 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>  -->
                <!-- step 2 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div> --> 
                <!-- step 3 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div>-->  
                <!-- step 4 -->
                <!--<div class="data">
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                    <div class="data-detail">
                    <p><strong>Strategically planning </strong>A proper strategic SMO planning is very important for the success of your business through the internet. The main aim of our strategic planning is to conceptualize and implement the client’s targets and objectives in a systematic manner. With our strategic planning, you are assured to get the best outcomes for web designing and marketing of your business. We assist you to analyse, create and execute custom made strategic plans that can fulfil the expectations and the requirements of your organisation. eTraffic Web Marketing helps you to include all unique prospects and the promotional services in your strategic planning to further expand your business.</p>
                    </div>
                </div> --> 
            </div>                
        </div>                         
            <a class="next">next</a>
            <div class="navi"></div>
        </div>
        <div class="shadow-bottom"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-bottom.png" alt="shadow-bottom"></div>
    </section>
  <?php } ?>


    <section class="content">
        <div class="wrapper">
    <p>eTraffic Web Marketing has a team of Web Professionals who have years of experience in the field of Web Marketing and Design. Each eTraffic professional is carefully chosen to make sure that we get only the people who have undying passion for the job.</p>
    <p>Below is a list of the various services offered by eTraffic Web Marketing to small and medium businesses who want to maximise the power of the internet.</p>
    
      <div class="like-work">
        <div class="like-work-wrap">
           Like to work with us to make your Business Bigger?
           <a href="#" class="btn">Contact Us</a>
        </div>
      </div>

        </div>
    </section>
</section>

<?php get_footer(); ?>


