<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content inner">
            <h1><?php
$category = get_the_category(); 
echo $category[0]->cat_name;
?></h1> 	

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>

				

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>

		<!--</div>-->
       </article>

<?php get_sidebar();?>
    </div>
</section>
<?php get_footer(); ?>
