<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<section class="two-column-right">
    <div class="wrapper">
        <article>
          <div id="primary" class="site-content">
            <h1><?php
$category = get_the_category(); 
echo $category[0]->cat_name;
?></h1>
            <?php if(have_posts()):
               while(have_posts()):the_post()?>
            <article>
            	<h3 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                <div class="post-meta">
                    <span class="date"><?php the_time('F j,Y');?></span>
                    <span class="author-name"><?php the_author();?></span>
                    <span class="comments"><?php
$category = get_the_category(); 
echo $category[0]->cat_name;
?></span>
                </div>
                <div class="post-thumb">
                    <a href="<?php the_permalink();?>"><?php the_post_thumbnail(array(320,213)); ?></a>
                </div>
                <div class="post-content">
                   <p><?php the_excerpt();?></p>
                   <!--<a href="blog-detail.php" class="reamore">Read More</a>-->
                 </div>                    
            </article>
        <?php endwhile;?>
    <?php endif;?>
            <?php wp_pagenavi();?>
          </div>
        </article>
        <?php get_sidebar();?>
    </div>
</section>

<?php get_footer(); ?>
