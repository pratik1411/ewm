<?php

/**

 * The template for displaying the footer.

 *

 * Contains footer content and the closing of the

 * #main and #page div elements.

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */

?>

	<!-- footer start -->

<footer class="grey">

	<div class="shadow-top"><img src="<?php echo bloginfo('template_directory');?>/images/shadow-top.png" alt="shadow-top"></div>

	<div class="wrapper">

    	

    	<div class="quick-links">

        <h3>Quick Links</h3>

        <ul>

            <li><a href="http://www.etrafficwebmarketing.com.au">Home</a></li>

            <li><a href="http://www.etrafficwebmarketing.com.au/about-us">About Us</a></li>

            <li><a href="http://www.etrafficwebmarketing.com.au/our-services">Our Services</a></li>

	       <!-- <li><a href="#">Our Packages</a></li> -->

            <li><a href="<?php bloginfo('url');?>">Our Blog</a></li>

            <li><a href="http://www.etrafficwebmarketing.com.au/contact-us">Contact US</a></li>

        </ul>

        </div>

        <div class="our-services">

        	<h3>Our services</h3>

        	<ul>

                <li><a href="http://www.etrafficwebmarketing.com.au/seo">Search Engine Optimisation</a></li>

                <li><a href="http://www.etrafficwebmarketing.com.au/search-engine-marketing">Search Engine Marketing</a></li>

                <li><a href="http://www.etrafficwebmarketing.com.au/social-media-optimisation">Social Media Optimisation</a></li>

                <li><a href="http://www.etrafficwebmarketing.com.au/ecommerce-seo">eCommerce SEO</a></li>

                <li><a href="http://www.etrafficwebmarketing.com.au/video-optimisation">Video Production</a></li>

            </ul>

        </div>

        <div class="contacts">

          <h3>Contact us</h3>

          <p>

            Level 1, 530 Little Collins Street <br>

            Melbourne VIC 3000<br>

            <strong>1300 887 151</strong><br>

            <a href="mailto:info@eTrafficWebMarketing.com.au" class="email">info@eTrafficWebMarketing.com.au</a>

          </p>

        </div>

        <div class="follow">

        	<h3>follow us</h3>

        	<ul>

            	<li><a href="https://www.facebook.com/" target="_blank" class="facebook">Facebook</a></li>

                <li><a href="https://twitter.com/etrafficgroup" target="_blank" class="twitter">Twitter</a></li>

                <li><a href="http://www.linkedin.com/in/cameronfrancis" target="_blank" class="in">Linkedin</a></li>

                <li><a href="http://www.youtube.com/user/MrCameronFrancis" target="_blank" class="youtube">Youtube</a></li>

                <li><a href="http://www.yelp.com.au/biz/the-etraffic-group-melbourne-2" target="_blank" class="yelp">Yelp</a></li>

                <li><a href="https://plus.google.com/111067911665788661802/posts" target="_blank" class="gplus">Google +</a></li>

               <li><a href="http://www.etrafficwebmarketing.com.au/blog/feed/" class="rss">RSS</a></li>

            </ul>

        </div>

        <div class="c"></div>        

    </div>    

    <div class="copy">Copyright &copy; <?php echo date('Y'); ?>. eTraffic Web Marketing. All right reserved</div>

</footer>

<!-- footer close -->

<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript">
   var _mfq = _mfq || [];
   (function() {
       var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
       mf.src = "//cdn.mouseflow.com/projects/a07e9f10-ec69-4807-8d49-851e92267a93.js";
       document.getElementsByTagName("head")[0].appendChild(mf);
   })();
</script>


<?php wp_footer(); ?>
<!-- begin olark code -->

<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){

f[z]=function(){

(a.s=a.s||[]).push(arguments)};var a=f[z]._={

},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){

f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={

0:+new Date};a.P=function(u){

a.p[u]=new Date-a.p[0]};function s(){

a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){

hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){

return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){

b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{

b.contentWindow[g].open()}catch(w){

c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{

var t=b.contentWindow[g];t.write(p());t.close()}catch(x){

b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({

loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});

/* custom configuration goes here (www.olark.com/documentation) */

olark.identify('9723-788-10-9050');/*]]>*/</script>
<noscript><a href="https://www.olark.com/site/9723-788-10-9050/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>

<!-- end olark code -->


</body>

</html>

