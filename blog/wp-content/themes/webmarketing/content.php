<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
        <h3 class="post-title">
        	<a href="#"><?php the_title();?></a>
        </h3>

        <div class="post-meta">
                <span class="date"><?php the_time('F j,Y');?></span>
                <span class="author-name"><?php the_author();?></span>
                <span class="comments"><?php
$category = get_the_category(); 
echo $category[0]->cat_name;
?></span>
	</div>
			<p><?php the_content();?></p>
	
		
			
			
		
	
