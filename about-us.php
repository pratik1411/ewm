<?php include('include/site.php'); ?>
<title>About eTraffic Web Marketing </title>
</head>
<body>
<?php include('include/header.php'); ?>
<section class="inner-page">
<section class="page-title">
    <div class="wrapper">
        <div class="page_title">About Us</div>    
        <span class="sub-title"><h1>Best SEO Company in Australia Enhances Your Global Recognition</h1></span>
    </div>
</section>
<section class="content">
	<div class="wrapper">
    	<div class="odd">
        	<img src="images/about-us.jpg" alt="about-us" class="alignright">
        	<p><strong>eTraffic Web Marketing is an online marketing company based out of Melbourne, Australia,  with secondary offices in Sydney, Brisbane, Darwin, Perth, Adelaide & Hobart.</strong></p>
            <p>Established in 2010, eTraffic provides specialist online marketing services such as:</p>
            <ul class="style1">
                <li>Search Engine Optimisation</li>
                <li>Pay Per Click</li>
                <li>Search Engine Marketing</li>
                <li>Reputation Management</li>
                <li>Social Media Optimisation</li>
                <li>Video Optimisation to small and large companies around the country.</li>
            </ul>
        </div> 
        <div class="even">
        	<img src="images/our-1-priority-is-you.jpg" alt="our-1-priority-is-you" class="alignleft">
        	<h2>Our #1 priority is you</h2>
            <p>Our #1 priority is to create, understand and maintain a successful online presence for all of our clients. We are able to do this by researching, understanding and implementing all of the latest search engine techniques that ensures long term success.</p>           
            <p><strong>We are every day SEO GEEK Rockstars</strong></p>
            <p>In every sense of the word. We have the knowledge, passion and expertise to deliver truly amasing results no matter the budget or industry</p>
        </div>        
        <?php include('include/gurantee.php'); ?>
    </div>    
</section>
<?php include('include/testimonial.php'); ?>
</section>
<?php include('include/footer.php'); ?>
</body>
</html>